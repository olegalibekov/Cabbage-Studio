// window.onValue = function (way) {
//     let ref = firebase.database().ref(way);
//     let path = `#onvalue#${way}`;
//     if (!window.listenersRef[path]) {
//
//         window.listenersRef[path] = ref;
//     }
// };


function dispatchEvent(path, data) {
    let event = new CustomEvent(path, {
        detail: data
    });
    document.dispatchEvent(event);
}

function activateStreamFlow(type, refPath) {
    return listenersRef[refPath].on(type, function (snapshot) {
        dispatchEvent(refPath, snapshot.val());
    });
}

window.listenersRef = {};
window.listenersInlineCode = {};

window.once = async function (path) {
    let ref = firebase.database().ref(path);
    let data = JSON.stringify((await ref.once('value')).val());
    return data;
}
window.set = function (path, data) {
    return firebase.database().ref(path).set(JSON.parse(data));
}
window.update = function (path, data) {
    return firebase.database().ref(path).update(JSON.parse(data));
}
window.onStream = function (type, path) {
    let refPath = `#${type}#${path}`;
    let ref = firebase.database().ref(path);
    if (!window.listenersRef[refPath]) {
        window.listenersRef[refPath] = ref;
        window.listenersInlineCode[refPath] = activateStreamFlow(type, refPath);
    } else {
        window.offStream(type, path);
        window.onStream(type, path);
    }
}
window.offStream = function (type, path) {
    let refPath = `#${type}#${path}`;
    window.listenersRef[refPath].off(type, window.listenersInlineCode[refPath]);
    delete window.listenersRef[refPath];
    delete window.listenersInlineCode[refPath];
}
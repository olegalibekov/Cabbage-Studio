window.putFile = async function (path, bytes) {
    let storageRef = firebase.storage().ref().child(path);
    await storageRef.put(bytes);
    return storageRef.getDownloadURL();
}
window.deleteFile = function (path) {
    let storageRef = firebase.storage().ref().child(path);
    return storageRef.delete();
}
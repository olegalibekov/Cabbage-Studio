import 'dart:math';

import 'package:cabbage_studio/custom_widgets/custom_drag_detector.dart';
import 'package:cabbage_studio/frames/menu_page.dart';
import 'package:cabbage_studio/main.dart';
import 'package:cabbage_studio/pages/admin/admin_page.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:cabbage_studio/utils/screen_sizes.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';
import 'package:zflutter/zflutter.dart';

class WelcomePage extends StatefulWidget {

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

var pageSize;

class _WelcomePageState extends State<WelcomePage>
    with TickerProviderStateMixin {
  var _screenOrientation;

  Pyramid _pyramid;
  Pyramid _phoneScreenSmallPyramid;
  Pyramid _phoneScreenLargePyramid;

  @override
  Widget build(BuildContext context) {
    pageSize = MediaQuery.of(context).size;
    if (_screenOrientation == null) {
      _screenOrientation = MediaQuery.of(context).orientation;
    }

    if (_pyramid == null) {
      _pyramid = Pyramid(1080 * 320 / 850, 1920 * 0.125, 3, 2, false);
    }

    final child = ZIllustration(children: [
      ZPositioned(
          translate:
              ZVector(pageSize.width * 0.064, -pageSize.height * 0.100, 0),
          rotate: ZVector(1.5, 1.25, 0.3),
          child: ZGroup(children: [
            ZPositioned(
                child: ZGroup(children: [
              ZPositioned(
                  child: _pyramid.withEdgesOpacityAndTitles(1.0, () {
                Navigator.push(
                    context,
                    EnterExitRoute(
                        exitPage: this.widget,
                        enterPage: isUser ? MenuPage() : AdminPage()));
              }))
            ]))
          ])),
      // ZPositioned(
      //     translate:
      //         ZVector(pageSize.width / 2 - 200, pageSize.height / 2 - 200, 0),
      //     child: ZToBoxAdapter(
      //         width: 200,
      //         height: 200,
      //         child: Row(children: [
      //           FlatButton(
      //               onPressed: () {
      //                 isUser = true;
      //               },
      //               child: Text('isUser')),
      //           FlatButton(
      //               onPressed: () {
      //                 isUser = false;
      //               },
      //               child: Text('isAdmin'))
      //         ]))),
    ]);

    if (pageSize.width < ScreenSizes.mobileWidth) {
      _phoneScreenSmallPyramid = Pyramid(100, 100, 3, 2, false);
      _phoneScreenLargePyramid = Pyramid(225, 100, 3, 2, false);

      return Scaffold(
          body: ZIllustration(children: [
        ZPositioned(
            translate: ZVector(MediaQuery.of(context).size.width / 2 - 50.0,
                MediaQuery.of(context).size.height / 2 - 250.0, 0),
            rotate: ZVector(pi / 2, 0.35, 0),
            child: ZGroup(children: [
              ZPositioned(
                  rotate: ZVector(-0.2, 0.0, 0.0),
                  child: _phoneScreenSmallPyramid.withEdgesOpacity(1.0))
            ])),
        ZPositioned(
            translate: ZVector(MediaQuery.of(context).size.width / 2 - 180.0,
                -MediaQuery.of(context).size.height / 2 + 100.0, 0.0),
            rotate: ZVector(pi / 2, pi / 2 - 0.4, 0),
            child: ZGroup(children: [
              ZPositioned(
                  rotate: ZVector(0.3, 0, 0.2),
                  child: _phoneScreenLargePyramid.withEdgesOpacity(1.0))
            ])),
        ZPositioned(
            translate: ZVector(MediaQuery.of(context).size.width / 2 - 150.0,
                -MediaQuery.of(context).size.height / 2 + 150.0, 0.0),
            child: ZToBoxAdapter(
                width: 98.0,
                height: 30.0,
                child: Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(18.0),
                    child: InkWell(
                        borderRadius: BorderRadius.circular(18.0),
                        onTap: () {
                          Navigator.push(
                              context,
                              EnterExitRoute(
                                  exitPage: this.widget,
                                  enterPage: MenuPage()));
                        },
                        child: Stack(
                            alignment: Alignment.centerLeft,
                            overflow: Overflow.visible,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                      // color: Colors.white,
                                      border: Border.all(
                                          color:
                                              Color.fromRGBO(227, 227, 227, 1),
                                          width: 1.5),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(18.0)))),
                              Positioned(
                                  left: 5,
                                  top: -10,
                                  child: Row(children: [
                                    Text('by',
                                        style: GoogleFonts.sourceCodePro(
                                            fontSize: 16,
                                            color:
                                                Colors.black.withOpacity(0.5))),
                                    Text('Cabbage\nStudio',
                                        style: TextStyle(
                                            fontFamily: 'SourceCodePro',
                                            fontSize: 20,
                                            fontStyle: FontStyle.italic,
                                            color:
                                                Colors.black.withOpacity(0.5),
                                            shadows: [
                                              Shadow(
                                                  offset: Offset(1, 0.6),
                                                  blurRadius: 1.0,
                                                  color: Colors.black)
                                            ]))
                                  ]))
                            ]))))),
        ZPositioned(
            translate: ZVector(
                -MediaQuery.of(context).size.width / 2 + 150.0 + 16.0,
                MediaQuery.of(context).size.height / 2 - 100.0,
                0.0),
            child: ZToBoxAdapter(
                width: 300,
                height: 140,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Welcome',
                          style: GoogleFonts.spaceMono(
                              // height: 0.1,
                              fontWeight: FontWeight.w800,
                              color: Colors.black,
                              fontSize: 64)),
                      SizedBox(height: 20),
                      RichText(
                          text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: 'lets explore ',
                            style: GoogleFonts.sourceCodePro(
                                // height: 0.1,
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.w300)),
                        TextSpan(
                            text: 'something new',
                            style: GoogleFonts.sourceCodePro(
                                // height: 0.1,
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.w600))
                      ]))
                    ])))
      ]));
    }
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        body: child);
  }
}

pageTitle(title) =>
    Text(title, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold));

import 'dart:html';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

class HeroProjectPage extends StatefulWidget {
  final project;

  const HeroProjectPage(this.project, {Key key}) : super(key: key);

  @override
  _HeroProjectPageState createState() => _HeroProjectPageState();
}

class _HeroProjectPageState extends State<HeroProjectPage> {
  Widget _iFrameWidget;

  initState() {
    super.initState();

    final IFrameElement _iFrameElement = IFrameElement();
    _iFrameElement.src = widget?.project;
    // _iFrameElement.src = 'https://the-cabbage-studio.web.app/projects/theater/';
    _iFrameElement.style.border = 'none';

    ui.platformViewRegistry
        .registerViewFactory('iframeElement', (int viewId) => _iFrameElement);

    _iFrameWidget =
        HtmlElementView(key: UniqueKey(), viewType: 'iframeElement');
  }

  Widget build(BuildContext context) {
    var _pageSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(children: [
      Hero(
          tag: 'heroAnimation',
          child: SizedBox(
              width: _pageSize.width,
              height: _pageSize.height,
              child: _iFrameWidget)),
      Positioned(
          left: 16.0,
          top: 16.0,
          child: PointerInterceptor(
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  })))
    ]));
    // return Scaffold(body: Hero(tag: 'heroAnimation', child: widget.content));
  }
}

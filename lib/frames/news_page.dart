import 'dart:math';

import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:zflutter/zflutter.dart' hide Axis;

import 'menu_page.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> with TickerProviderStateMixin {
  Pyramid _pyramidRight;
  ShapeData _pyramidRightShapeData;
  final Random _random = Random();

  var _news = db.root.globalinfo.news.stream;

  @override
  void initState() {
    var _pyramidRightHeight = 300.0;
    _pyramidRight = Pyramid(225, _pyramidRightHeight, 3, 2, false);

    AnimationController _pyramidRightTranslationController =
        AnimationController.unbounded(vsync: this);
    const _pyramidRightSpring =
        SpringDescription(mass: 1, stiffness: 1, damping: 0);
    Simulation _pyramidRightTranslationSimulation =
        SpringSimulation(_pyramidRightSpring, 0, 1, 0);
    Tween _pyramidRightTween = Tween(begin: Offset(0, 0), end: Offset(0, 0));
    Animation _pyramidRightTranslationAnimation =
        _pyramidRightTranslationController.drive(_pyramidRightTween);
    AnimationController _pyramidRightAccelerationController =
        AnimationController(vsync: this);
    AnimationController _pyramidRightRotationController =
        AnimationController(vsync: this);

    /// Was Tween(begin: _currentVelocity, end: acceleration)
    Tween _pyramidRightAccelerationTween = Tween(begin: 0.0, end: 0.0);
    Animation _pyramidRightAccelerationAnimation =
        _pyramidRightAccelerationTween
            .animate(_pyramidRightAccelerationController);
    double _pyramidRightCurrentRotation = 0.0;

    _pyramidRightShapeData = ShapeData(
        _pyramidRightTranslationController,
        _pyramidRightTranslationAnimation,
        _pyramidRightTranslationSimulation,
        _pyramidRightRotationController,
        _pyramidRightAccelerationController,
        _pyramidRightAccelerationAnimation,
        _pyramidRightTween,
        _pyramidRightAccelerationTween,
        _pyramidRightCurrentRotation,
        _pyramidRight,
        _pyramidRightHeight,
        _pyramidRight.pyramidVolume());

    double _rightPyramidRandomX = _random.nextInt(30) - 15.0;
    double _rightPyramidRandomY = _random.nextInt(30) - 15.0;
    _pyramidRightShapeData.runTranslation(
        _pyramidRightShapeData.offsetAnimation,
        Offset(_rightPyramidRandomX, _rightPyramidRandomY));
    _pyramidRightShapeData
        .accelerationRotation(_pyramidRightShapeData.neutralVelocity);

    super.initState();
  }

  @override
  void dispose() {
    disposeControllers([_pyramidRightShapeData]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ZTransform _pyramidRightStartedZPositioned = ZTransform(
        translate: ZVector(
            -MediaQuery.of(context).size.width / 2 +
                MediaQuery.of(context).size.width * 1060 / 1100,
            MediaQuery.of(context).size.height * 0.175,
            0),
        rotate: ZVector(pi / 2, pi / 2 - 1.5, 0.09));
    _pyramidRightShapeData.startedZTransform = _pyramidRightStartedZPositioned;
    return Scaffold(
        body: Stack(children: [
      ZIllustration(children: [
        AnimatedBuilder(
            animation: Listenable.merge([
              _pyramidRightShapeData.translationController,
              _pyramidRightShapeData.rotationController
            ]),
            builder: (context, child) {
              if (_pyramidRightShapeData.translationAnimation?.value != null) {
                _pyramidRightShapeData.offsetAnimation =
                    _pyramidRightShapeData.translationAnimation.value;
              }

              return ZPositioned(
                  translate: _pyramidRightShapeData.startedZTransform.translate,
                  rotate: _pyramidRightShapeData.startedZTransform.rotate,
                  child: ZGroup(children: [
                    ZPositioned(
                        translate: ZVector(
                            _pyramidRightShapeData.offsetAnimation.dx,
                            _pyramidRightShapeData.offsetAnimation.dy,
                            0),
                        // rotate: controller.rotate,
                        rotate: ZVector(
                            _pyramidRightShapeData.currentRotation, 0, 0),
                        child: ZGroup(children: [
                          ZPositioned(
                              translate: ZVector(
                                  0, _pyramidRightShapeData.shapeHeight / 3, 0),
                              child: _pyramidRight.withEdgesOpacity(1.0))
                        ]))
                  ]));
            })
      ]),
      Positioned(top: 24.0, left: 52.0, child: byCabbageStudio(true,() {
        goToPage(context, this.widget, MenuPage());
      })),
      Align(
          alignment: Alignment.center,
          child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: StreamBuilder(
                  stream: _news,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    var allNews = snapshot?.data?.values ?? [];
                    var allNewsVisual = [];
                    var allTagsVisual = [];

                    if (allNews != []) {
                      for (News news in allNews) {
                        allNewsVisual.add(NewsCard(news));

                        final split = news.tags.value.split(',');
                        for (int i = 0; i < split.length; i++) {
                          if (split[i] != 'null') {
                            allTagsVisual.add(Chip(
                                label: Text(
                                    split[i].replaceAll(RegExp(r"\s+"), ""))));
                          }
                        }
                      }
                    }

                    return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                              width: 500.0,
                              child: ListView(
                                  physics: BouncingScrollPhysics(),
                                  children: [
                                    SizedBox(height: 48.0),
                                    ...allNewsVisual,
                                    SizedBox(width: 64.0),
                                    SizedBox(height: 48.0),
                                  ])),
                          SizedBox(width: 48),
                          ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth: 100, maxWidth: 400.0),
                              child: Wrap(
                                  spacing: 12.0,
                                  runSpacing: 12.0,
                                  alignment: WrapAlignment.start,
                                  children: [...allTagsVisual]))
                        ]);
                  }))),
      Positioned(
          left: 64.0, bottom: 24.0, child: bottomTitle(context, ': Новости'))
    ]));
  }
}

class NewsCard extends StatefulWidget {
  final News news;

  const NewsCard(this.news, {Key key}) : super(key: key);

  @override
  _NewsCardState createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 14.0, bottom: 14.0),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                border: Border.all(
                    width: 4.0, color: Colors.grey.withOpacity(0.5))),
            child: Column(children: [
              ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(6.0),
                      topRight: Radius.circular(6.0)),
                  child: CachedNetworkImage(
                      imageUrl: widget.news.links.value,
                      width: 500.0,
                      // placeholder: (context, url) =>
                      //     CircularProgressIndicator(),
                      // errorWidget: (context, url, error) =>
                      //     Center(child: Icon(Icons.error))
                  )),
              SizedBox(height: 12.0),
              if (widget.news.tags.value != null)
                Builder(
                  builder: (BuildContext context) {
                    var tags = [];
                    // final tagName = '#grubs, #sheep';
                    final split = widget.news.tags.value.split(',');
                    for (int i = 0; i < split.length; i++) {
                      tags.add(Chip(
                          label:
                              Text(split[i].replaceAll(RegExp(r"\s+"), ""))));
                    }

                    return Wrap(
                        spacing: 12.0,
                        runSpacing: 12.0,
                        alignment: WrapAlignment.start,
                        children: [...tags]);
                  },
                ),
              // if (widget.news.tags.value != 'null') SizedBox(height: 16.0),
              // SizedBox(width:10,height:2,child: Divider(height: 2.0,color: Colors.black)),
              SizedBox(height: 16),
              // SizedBox(width:10,height:2,child: Divider(height: 2.0,color: Colors.black)),
              Container(
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Theme.of(context).dividerColor,
                              width: 1.0)))),
              // SizedBox(height: 16.0),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(widget.news.title.value,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                          fontSize: 20.0))),
              Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16.0, bottom: 16.0),
                  child: Text(widget.news.shortDescription.value,
                      style: TextStyle(fontSize: 16.0)))
            ])));
  }
}

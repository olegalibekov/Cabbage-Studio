import 'dart:math';

import 'package:cabbage_studio/frames/news_page.dart';
import 'package:cabbage_studio/frames/personal_account_page.dart';
import 'package:cabbage_studio/frames/portfolio_page.dart';
import 'package:cabbage_studio/pages/admin/admin_projects_page.dart';
import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:cabbage_studio/utils/screen_sizes.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

final boxBottomContentQuotient = 1.5;

class _MenuPageState extends State<MenuPage> with TickerProviderStateMixin {
  final _boxSize = Size(180.0, 48.0);

  Pyramid _phoneScreenPyramid;
  Pyramid _pyramidRight;
  Pyramid _pyramidLeft;

  ShapeData _phoneScreenPyramidShapeData;
  ShapeData _pyramidRightShapeData;
  ShapeData _pyramidLeftShapeData;

  final Random _random = Random();

  List<ShapeData> _shapeList = [];

  drawerItem(Size contentSize, Widget content, drawerTitle,
      [drawerImplementation]) {
    Offset drawerOffset = Offset(-11.0, -12.0);
    Size drawerSize = Size(175, 50);
    final stroke = 1.5;
    return Container(
        width: contentSize.width,
        height: contentSize.height + _boxSize.height,
        child: Stack(overflow: Overflow.visible, children: [
          SizedBox(
              width: contentSize.width,
              height: contentSize.height,
              child: content),
          Positioned(
              left: drawerOffset.dx - 1.0,
              bottom: -stroke * 2,
              child: Container(
                  // color: Colors.red,
                  width: drawerSize.width - drawerOffset.dx + 3,
                  height: drawerSize.height - drawerOffset.dy + 3,
                  child: ZIllustration(children: [
                    ZPositioned(
                        translate: ZVector(
                            -drawerOffset.dx / 2, -drawerOffset.dy / 2, 0),
                        child: BoxElement(
                            drawerTitle,
                            drawerSize.width,
                            drawerSize.height,
                            drawerOffset,
                            null,
                            stroke,
                            drawerImplementation))
                  ])))
        ]));
  }

  AnimationController _phoneScreenPyramidTranslationController;

  @override
  void initState() {
    var _phoneScreenPyramidHeight = 200.0;
    _phoneScreenPyramid =
        Pyramid(150.0, _phoneScreenPyramidHeight, 3, 2, false);

    _phoneScreenPyramidTranslationController =
        AnimationController.unbounded(vsync: this);
    const _phoneScreenPyramidSpring =
        SpringDescription(mass: 1, stiffness: 1, damping: 0);
    Simulation _phoneScreenPyramidTranslationSimulation =
        SpringSimulation(_phoneScreenPyramidSpring, 0, 1, 0);
    Tween _phoneScreenPyramidTween =
        Tween(begin: Offset(0, 0), end: Offset(0, 0));
    Animation _phoneScreenPyramidTranslationAnimation =
        _phoneScreenPyramidTranslationController
            .drive(_phoneScreenPyramidTween);
    AnimationController _phoneScreenPyramidAccelerationController =
        AnimationController(vsync: this);
    AnimationController _phoneScreenPyramidRotationController =
        AnimationController(vsync: this);

    /// Was Tween(begin: _currentVelocity, end: acceleration)
    Tween _phoneScreenPyramidAccelerationTween = Tween(begin: 0.0, end: 0.0);
    Animation _phoneScreenPyramidAccelerationAnimation =
        _phoneScreenPyramidAccelerationTween
            .animate(_phoneScreenPyramidAccelerationController);
    double _phoneScreenPyramidCurrentRotation = 0.0;

    _phoneScreenPyramidShapeData = ShapeData(
        _phoneScreenPyramidTranslationController,
        _phoneScreenPyramidTranslationAnimation,
        _phoneScreenPyramidTranslationSimulation,
        _phoneScreenPyramidRotationController,
        _phoneScreenPyramidAccelerationController,
        _phoneScreenPyramidAccelerationAnimation,
        _phoneScreenPyramidTween,
        _phoneScreenPyramidAccelerationTween,
        _phoneScreenPyramidCurrentRotation,
        _phoneScreenPyramid,
        _phoneScreenPyramidHeight,
        _phoneScreenPyramid.pyramidVolume());

    _shapeList.add(_phoneScreenPyramidShapeData);

    var _pyramidRightHeight = 300.0;
    _pyramidRight = Pyramid(200, _pyramidRightHeight, 3, 2, false);

    AnimationController _pyramidRightTranslationController =
        AnimationController.unbounded(vsync: this);
    const _pyramidRightSpring =
        SpringDescription(mass: 1, stiffness: 1, damping: 0);
    Simulation _pyramidRightTranslationSimulation =
        SpringSimulation(_pyramidRightSpring, 0, 1, 0);
    Tween _pyramidRightTween = Tween(begin: Offset(0, 0), end: Offset(0, 0));
    Animation _pyramidRightTranslationAnimation =
        _pyramidRightTranslationController.drive(_pyramidRightTween);
    AnimationController _pyramidRightAccelerationController =
        AnimationController(vsync: this);
    AnimationController _pyramidRightRotationController =
        AnimationController(vsync: this);

    /// Was Tween(begin: _currentVelocity, end: acceleration)
    Tween _pyramidRightAccelerationTween = Tween(begin: 0.0, end: 0.0);
    Animation _pyramidRightAccelerationAnimation =
        _pyramidRightAccelerationTween
            .animate(_pyramidRightAccelerationController);
    double _pyramidRightCurrentRotation = 0.0;

    _pyramidRightShapeData = ShapeData(
        _pyramidRightTranslationController,
        _pyramidRightTranslationAnimation,
        _pyramidRightTranslationSimulation,
        _pyramidRightRotationController,
        _pyramidRightAccelerationController,
        _pyramidRightAccelerationAnimation,
        _pyramidRightTween,
        _pyramidRightAccelerationTween,
        _pyramidRightCurrentRotation,
        _pyramidRight,
        _pyramidRightHeight,
        _pyramidRight.pyramidVolume());

    _shapeList.add(_pyramidRightShapeData);

    var _pyramidLeftHeight = 300.0;
    _pyramidLeft = Pyramid(300, _pyramidLeftHeight, 3, 2, false);

    AnimationController _pyramidLeftTranslationController =
        AnimationController.unbounded(vsync: this);
    const _pyramidLeftSpring =
        SpringDescription(mass: 1, stiffness: 1, damping: 0);
    Simulation _pyramidLeftTranslationSimulation =
        SpringSimulation(_pyramidLeftSpring, 0, 1, 0);
    Tween _pyramidLeftTween = Tween(begin: Offset(0, 0), end: Offset(0, 0));
    Animation _pyramidLeftTranslationAnimation =
        _pyramidLeftTranslationController.drive(_pyramidLeftTween);
    AnimationController _pyramidLeftAccelerationController =
        AnimationController(vsync: this);
    AnimationController _pyramidLeftRotationController =
        AnimationController(vsync: this);

    /// Was Tween(begin: _currentVelocity, end: acceleration)
    Tween _pyramidLeftAccelerationTween = Tween(begin: 0.0, end: 0.0);
    Animation _pyramidLeftAccelerationAnimation = _pyramidLeftAccelerationTween
        .animate(_pyramidLeftAccelerationController);
    double _pyramidLeftCurrentRotation = 0.0;

    _pyramidLeftShapeData = ShapeData(
        _pyramidLeftTranslationController,
        _pyramidLeftTranslationAnimation,
        _pyramidLeftTranslationSimulation,
        _pyramidLeftRotationController,
        _pyramidLeftAccelerationController,
        _pyramidLeftAccelerationAnimation,
        _pyramidLeftTween,
        _pyramidLeftAccelerationTween,
        _pyramidLeftCurrentRotation,
        _pyramidLeft,
        _pyramidLeftHeight,
        _pyramidLeft.pyramidVolume());

    _shapeList.add(_pyramidLeftShapeData);

    double _shapeListMinVolume = extremeVolume(_shapeList, false);
    double _shapeListMaxVolume = extremeVolume(_shapeList, true);
    if (_shapeList.length > 1) {
      /// Two lines were above

      for (int shapeIndex = 0; shapeIndex < _shapeList.length; shapeIndex++) {
        var shape = _shapeList[shapeIndex];

        double shapeDecelerationQuotient =
            (shape.shapeVolume - _shapeListMinVolume) /
                (_shapeListMaxVolume - _shapeListMinVolume);
        double shapeMaxVelocity = minVelocity +
            (1 - shapeDecelerationQuotient) * (maxVelocity - minVelocity);

        shape.shapeMaxVelocity = shapeMaxVelocity;
        shape.shapeDecelerationQuotient = shapeDecelerationQuotient;
      }
    }

    double _phoneScreenPyramidRandomX = _random.nextInt(30) - 15.0;
    double _phoneScreenPyramidRandomY = _random.nextInt(30) - 15.0;
    _phoneScreenPyramidShapeData.runTranslation(
        _phoneScreenPyramidShapeData.offsetAnimation,
        Offset(_phoneScreenPyramidRandomX, _phoneScreenPyramidRandomY));
    _phoneScreenPyramidShapeData
        .accelerationRotation(-_phoneScreenPyramidShapeData.neutralVelocity);

    double _rightPyramidRandomX = _random.nextInt(30) - 15.0;
    double _rightPyramidRandomY = _random.nextInt(30) - 15.0;
    _shapeList[0].runTranslation(_shapeList[0].offsetAnimation,
        Offset(_rightPyramidRandomX, _rightPyramidRandomY));
    _shapeList[0].accelerationRotation(_shapeList[0].neutralVelocity);

    double _leftPyramidRandomX = _random.nextInt(30) - 15.0;
    double _leftPyramidRandomY = _random.nextInt(30) - 15.0;
    _shapeList[1].runTranslation(_shapeList[1].offsetAnimation,
        Offset(_leftPyramidRandomX, _leftPyramidRandomY));
    _shapeList[1].accelerationRotation(-_shapeList[1].neutralVelocity);
    super.initState();
  }

  @override
  void dispose() {
    disposeControllers(_shapeList);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < ScreenSizes.mobileWidth) {
      ZTransform _phoneScreenPyramidStartedZPositioned = ZTransform(
          translate: ZVector(
              -MediaQuery.of(context).size.width / 2 +
                  MediaQuery.of(context).size.width * 935 / 1100,
              -MediaQuery.of(context).size.height * 0.4,
              0),
          rotate: ZVector(pi / 2, pi / 2 - 1.5, 0.09));
      _phoneScreenPyramidShapeData.startedZTransform =
          _phoneScreenPyramidStartedZPositioned;
      return Scaffold(
          body: Stack(children: [
        ZIllustration(children: [
          ZGroup(sortMode: SortMode.stack, children: [
            AnimatedBuilder(
                animation: Listenable.merge([
                  _phoneScreenPyramidShapeData.translationController,
                  _phoneScreenPyramidShapeData.rotationController
                ]),

                // _shapeList[shapeIndex].shape,

                builder: (context, child) {
                  if (_phoneScreenPyramidShapeData
                          .translationAnimation?.value !=
                      null) {
                    _phoneScreenPyramidShapeData.offsetAnimation =
                        _phoneScreenPyramidShapeData.translationAnimation.value;
                  }

                  return ZPositioned(
                      translate: _phoneScreenPyramidShapeData
                          .startedZTransform.translate,
                      rotate:
                          _phoneScreenPyramidShapeData.startedZTransform.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            translate: ZVector(
                                _phoneScreenPyramidShapeData.offsetAnimation.dx,
                                _phoneScreenPyramidShapeData.offsetAnimation.dy,
                                0),
                            // rotate: controller.rotate,
                            rotate: ZVector(
                                _phoneScreenPyramidShapeData.currentRotation,
                                0,
                                0),
                            child: ZGroup(children: [
                              ZPositioned(
                                  translate: ZVector(
                                      0,
                                      _phoneScreenPyramidShapeData.shapeHeight /
                                          3,
                                      0),
                                  child:
                                      _phoneScreenPyramid.withEdgesOpacity(1.0))
                            ]))
                      ]));
                })
          ])
        ]),
        Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(children: [
              SizedBox(height: 40),
              Row(
                children: [
                  SizedBox(
                      width: 110.0,
                      height: 40.0,
                      child: Material(
                        color: Colors.white,
                        child: InkWell(
                          borderRadius: BorderRadius.circular(18.0),
                          onTap: null,
                          //     () {
                          //   // Navigator.push(
                          //   //     context,
                          //   //     EnterExitRoute(
                          //   //         exitPage: this.widget,
                          //   //         enterPage: MenuPage()));
                          // },
                          child: Stack(
                              alignment: Alignment.centerLeft,
                              overflow: Overflow.visible,
                              children: [
                                Container(
                                    decoration: BoxDecoration(
                                        // color: Colors.white,
                                        border: Border.all(
                                            color: Color.fromRGBO(
                                                227, 227, 227, 1),
                                            width: 2.0),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(26.0)))),
                                Positioned(
                                    left: 5.0,
                                    top: -10.0,
                                    child: Row(children: [
                                      Text('by',
                                          style: GoogleFonts.sourceCodePro(
                                              fontSize: 18,
                                              color: Colors.black
                                                  .withOpacity(0.5))),
                                      Text('Cabbage\nStudio',
                                          style: TextStyle(
                                              fontFamily: 'SourceCodePro',
                                              fontSize: 24.0,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.w600,
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                              shadows: [
                                                Shadow(
                                                    offset: Offset(1, 0.6),
                                                    blurRadius: 1.0,
                                                    color: Colors.black)
                                              ]))
                                    ])),
                              ]),
                        ),
                      )),
                  Spacer(),
                  Material(
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.white,
                    child: InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                    color: Color.fromRGBO(227, 227, 227, 1.0))),
                            child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Icon(Icons.clear)))),
                  ),
                ],
              ),
              SizedBox(height: 40),
              Expanded(
                  child: CustomScrollView(
                      physics: BouncingScrollPhysics(),
                      slivers: [
                    SliverToBoxAdapter(child: SizedBox(height: 50)),
                    SliverToBoxAdapter(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          Text('Choose',
                              style: GoogleFonts.spaceMono(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 64,
                                // height: 0.1
                              )),
                          SizedBox(height: 52.0),
                          Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('what',
                                    style: GoogleFonts.spaceMono(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 29,
                                      // height: 0.1
                                    )),
                                SizedBox(width: 22.0),
                                Text('you',
                                    style: GoogleFonts.roboto(
                                      color: Colors.black,
                                      fontStyle: FontStyle.italic,
                                      fontSize: 36,
                                      // height: 0.1
                                    )),
                                SizedBox(width: 22.0),
                                Text('are',
                                    style: GoogleFonts.spaceMono(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontStyle: FontStyle.italic,
                                      fontSize: 36,
                                      // height: 0.1
                                    ))
                              ]),
                          SizedBox(height: 22.0),
                          Text('looking for',
                              style: GoogleFonts.sourceCodePro(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 18,
                                  height: 0.1))
                        ])),
                    SliverToBoxAdapter(child: SizedBox(height: 52)),
                    SliverPadding(
                      padding: const EdgeInsets.only(right: 18),
                      sliver: SliverList(
                          delegate: SliverChildListDelegate([
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(
                                Size(double.infinity, 175.0),
                                Container(color: Colors.cyan),
                                'Личный кабинет', () {
                              goToPage(
                                  context, this.widget, PersonalAccountPage());
                            })),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(
                                Size(double.infinity, 175.0),
                                Container(color: Colors.yellow),
                                'Портфолио', () {
                              // goToPage(context, this.widget, PortfolioPage());
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PortfolioPage()));
                            })),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(Size(double.infinity, 175.0),
                                Container(color: Colors.green), 'Калькулятор')),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(Size(double.infinity, 175.0),
                                Container(color: Colors.purple), 'О команде')),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(
                                Size(double.infinity, 175.0),
                                Container(color: Colors.indigoAccent),
                                'Новости')),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 36),
                            child: drawerItem(
                                Size(double.infinity, 175.0),
                                Container(color: Colors.orange),
                                'Быстрая навигация')),
                      ])),
                    ),
                    SliverToBoxAdapter(child: SizedBox(height: 60)),
                  ]))
            ])),
        Positioned(
            bottom: 0,
            left: 0,
            child: Container(
                height: 80.0,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    gradient: LinearGradient(
                        begin: FractionalOffset.topLeft,
                        end: FractionalOffset.bottomRight,
                        colors: [
                          Colors.white,
                          Colors.white.withOpacity(0.3),
                        ],
                        stops: [
                          0.35,
                          0.9
                        ])))),
        Positioned(
            left: 16.0,
            bottom: 36.0,
            child: Text(': Меню',
                style: GoogleFonts.sourceCodePro(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 18))),
      ]));
    }

    ZTransform _pyramidLeftStartedZPositioned = ZTransform(
        translate: ZVector(
            -MediaQuery.of(context).size.width / 2 +
                MediaQuery.of(context).size.width * 0.05,
            0,
            0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _pyramidLeftShapeData.startedZTransform = _pyramidLeftStartedZPositioned;

    ZTransform _pyramidRightStartedZPositioned = ZTransform(
        translate: ZVector(
            -MediaQuery.of(context).size.width / 2 +
                MediaQuery.of(context).size.width * 935 / 1100,
            -MediaQuery.of(context).size.height * 0.25,
            0),
        rotate: ZVector(pi / 2, pi / 2 - 1.5, 0.09));
    _pyramidRightShapeData.startedZTransform = _pyramidRightStartedZPositioned;
    return Scaffold(
        body: Stack(children: [
      Positioned(
          top: MediaQuery.of(context).size.height * 20 / 570,
          left: MediaQuery.of(context).size.width * 40 / 1040,
          child: SizedBox(
              width: 110.0,
              height: 40.0,
              child: Stack(
                  alignment: Alignment.centerLeft,
                  overflow: Overflow.visible,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Color.fromRGBO(227, 227, 227, 1),
                                width: 2.0),
                            borderRadius:
                                BorderRadius.all(Radius.circular(26.0)))),
                    Positioned(
                        left: 5.0,
                        top: -10.0,
                        child: Row(children: [
                          Text('by',
                              style: GoogleFonts.sourceCodePro(
                                  fontSize: 18,
                                  color: Colors.black.withOpacity(0.5))),
                          Text('Cabbage\nStudio',
                              style: TextStyle(
                                  fontFamily: 'SourceCodePro',
                                  fontSize: 24.0,
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black.withOpacity(0.5),
                                  shadows: [
                                    Shadow(
                                        offset: Offset(1, 0.6),
                                        blurRadius: 1.0,
                                        color: Colors.black)
                                  ]))
                        ]))
                  ]))),
      Positioned(
          top: MediaQuery.of(context).size.height * 127 / 570,
          left: 270,
          child: Stack(overflow: Overflow.visible,
              // mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Choose',
                    style: GoogleFonts.spaceMono(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 64,
                      // height: 0.1
                    )),
                // SizedBox(height: 0.0),
                Positioned(
                  top: 87.0,
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('what',
                            style: GoogleFonts.spaceMono(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 29,
                              // height: 0.1
                            )),
                        SizedBox(width: 22.0),
                        Text('you',
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontStyle: FontStyle.italic,
                              fontSize: 36,
                              // height: 0.1
                            )),
                        SizedBox(width: 22.0),
                        Text('are',
                            style: GoogleFonts.spaceMono(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize: 36,
                              // height: 0.1
                            ))
                      ]),
                ),
                // SizedBox(height: 22.0),
                Positioned(
                  top: 132.0,
                  child: Text('looking for',
                      style: GoogleFonts.sourceCodePro(
                        color: Colors.black,
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        // height: 0.1
                      )),
                )
              ])),
      Positioned(
          left: MediaQuery.of(context).size.width * 55 / 1040,
          bottom: MediaQuery.of(context).size.height * 18 / 570,
          child: Text(': Меню',
              style: GoogleFonts.sourceCodePro(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 18))),
      ZIllustration(children: [
        ZGroup(sortMode: SortMode.stack, children: [
          for (int shapeIndex = 1;
              shapeIndex < 2;
              // shapeIndex < 1;
              shapeIndex++)
            AnimatedBuilder(
                animation: Listenable.merge([
                  _shapeList[shapeIndex].translationController,
                  _shapeList[shapeIndex].rotationController
                ]),

                // _shapeList[shapeIndex].shape,

                builder: (context, child) {
                  if (_shapeList[shapeIndex].translationAnimation?.value !=
                      null) {
                    _shapeList[shapeIndex].offsetAnimation =
                        _shapeList[shapeIndex].translationAnimation.value;
                  }

                  return ZPositioned(
                      translate:
                          _shapeList[shapeIndex].startedZTransform.translate,
                      rotate: _shapeList[shapeIndex].startedZTransform.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            translate: ZVector(
                                _shapeList[shapeIndex].offsetAnimation.dx,
                                _shapeList[shapeIndex].offsetAnimation.dy,
                                0),
                            // rotate: controller.rotate,
                            rotate: ZVector(
                                _shapeList[shapeIndex].currentRotation, 0, 0),
                            child: ZGroup(children: [
                              ZPositioned(
                                  translate: ZVector(
                                      0,
                                      _shapeList[shapeIndex].shapeHeight / 3,
                                      0),
                                  child: _pyramidRight.withEdgesOpacity(1.0))
                            ]))
                      ]));
                }),
          // ZPositioned(
          //     translate: ZVector(350, -100, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               double _randomX = _random.nextInt(30) - 15.0;
          //               double _randomY = _random.nextInt(30) - 15.0;
          //               _shapeList[0].runTranslation(
          //                   _shapeList[0].offsetAnimation,
          //                   Offset(_randomX, _randomY));
          //             },
          //             child: Text('Change direction')))),
          // ZPositioned(
          //     translate: ZVector(350, 0, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               _shapeList[0].runTranslation(
          //                   _shapeList[0].offsetAnimation,
          //                   Offset(-25.0, 50.0));
          //             },
          //             child: Text('Animate')))),
          // ZPositioned(
          //     translate: ZVector(350, 100, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               _shapeList[0].accelerationRotation(
          //                   _shapeList[0].neutralVelocity);
          //             },
          //             child: Text('Passive forward')))),
          // ZPositioned(
          //     translate: ZVector(350, 200, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               _shapeList[1].accelerationRotation(
          //                   -_shapeList[1].neutralVelocity);
          //             },
          //             child: Text('Passive back')))),
          // ZPositioned(
          //     translate: ZVector(350, 300, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               _shapeList[0].accelerationRotation(
          //                   _shapeList[0].shapeMaxVelocity);
          //             },
          //             child: Text('Acceleration forward')))),
          // ZPositioned(
          //     translate: ZVector(150, 300, 0),
          //     child: ZToBoxAdapter(
          //         width: 200,
          //         height: 100,
          //         child: FlatButton(
          //             onPressed: () {
          //               _shapeList[0].accelerationRotation(
          //                   -_shapeList[0].shapeMaxVelocity);
          //             },
          //             child: Text('Acceleration back')))),

          ZPositioned(
              // translate: ZVector(
              //     -MediaQuery.of(context).size.width / 2 +
              //         MediaQuery.of(context).size.height * 300 / 570,
              //     100,
              //     0),
              translate:
                  ZVector(-MediaQuery.of(context).size.width / 2 + 550, 125, 0),
              // top: MediaQuery.of(context).size.height * 127 / 570,
              // left: MediaQuery.of(context).size.width * 163 / 1040,
              child: ZToBoxAdapter(
                  width: MediaQuery.of(context).size.width,
                  height: 550,
                  child: Center(
                      child: SingleChildScrollView(
                          child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * 0.4),
                              child: Wrap(children: [
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'Личный кабинет',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.cyan),
                                          1.0, () {
                                        goToPage(context, this.widget,
                                            PersonalAccountPage());
                                      })
                                    ])),
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'Портфолио',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.yellow),
                                          1.0, () {
                                        // goToPage(context, this.widget,
                                        //     PortfolioPage());
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    PortfolioPage()));
                                      })
                                    ])),
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'Калькулятор',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.green))
                                    ])),
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'О команде',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.purple))
                                    ])),
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'Новости',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.indigoAccent),
                                          1.0, () {
                                        goToPage(
                                            context, this.widget, NewsPage());
                                      })
                                    ])),
                                SizedBox(
                                    width: 220,
                                    height: 200,
                                    child: ZIllustration(children: [
                                      BoxElement(
                                          'Быстрая навигация',
                                          _boxSize.width,
                                          _boxSize.height,
                                          Offset(-11.0, -12.0),
                                          Container(color: Colors.orange))
                                    ]))
                              ]))))))
        ])
      ])
    ]));
  }
}

class BoxElement extends StatefulWidget {
  final String title;
  final double boxWidth;
  final double boxHeight;
  final Offset frontBoxOpenOffset;
  final dynamic bottomContent;
  final double drawerStroke;
  final Function drawerOnTap;

  const BoxElement(
    this.title,
    this.boxWidth,
    this.boxHeight,
    this.frontBoxOpenOffset, [
    this.bottomContent,
    this.drawerStroke = 1.0,
    this.drawerOnTap,
    Key key,
  ]) : super(key: key);

  @override
  _BoxElementState createState() => _BoxElementState();
}

class _BoxElementState extends State<BoxElement>
    with SingleTickerProviderStateMixin {
  var _bottomContentHeight;

  AnimationController _controller;
  Animation _animation;
  Tween _tween;

  var _isOpened = false;

  @override
  void initState() {
    _bottomContentHeight = widget.boxHeight * boxBottomContentQuotient;

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    _tween = Tween(begin: Offset(0.0, 0.0), end: widget.frontBoxOpenOffset);
    _animation = _tween.animate(
        CurvedAnimation(curve: Curves.easeInQuint, parent: _controller));

    _controller.addStatusListener((status) async {
      if (status == AnimationStatus.completed &&
          _animation.value == widget.frontBoxOpenOffset) {
        await Future.delayed(Duration(milliseconds: 300));
        animationReverse();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context, Widget child) {
          return ZGroup(children: [
            // Box
            // ZToBoxAdapter(
            //     width: widget.boxWidth + 1,
            //     height: widget.boxHeight + 1,
            //     child: Container(color: Colors.white)),
            ZShape(
                path: [
                  // Front top left
                  ZMove(-widget.boxWidth / 2 + _animation.value.dx,
                      -widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Front top right
                  ZLine(widget.boxWidth / 2 + _animation.value.dx,
                      -widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Front bottom right
                  ZLine(widget.boxWidth / 2 + _animation.value.dx,
                      widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Front bottom left
                  ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                      widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Return front top left
                  ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                      -widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  //Connection between back bottom left and front bottom left
                  ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                      widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  //Connection between back bottom left and front bottom line
                  ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  ZLine(-widget.boxWidth / 2,
                      widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Connection between bottom lines
                  ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  ZLine(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  // Connection between back bottom right and front bottom right
                  ZMove(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  ZLine(widget.boxWidth / 2 + _animation.value.dx,
                      widget.boxHeight / 2 + _animation.value.dy, 0.0),
                  // Connection between back bottom right and back top right
                  ZMove(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
                  ZLine(widget.boxWidth / 2, -widget.boxHeight / 2, 0.0),
                  // Connection between back top right and front top left
                  ZMove(widget.boxWidth / 2, -widget.boxHeight / 2, 0.0),
                  ZLine(widget.boxWidth / 2 + _animation.value.dx,
                      -widget.boxHeight / 2, 0.0),
                ],
                color: Colors.black,
                stroke: widget.drawerStroke,
                closed: false,
                fill: true),

            ZPositioned(
                translate:
                    ZVector(_animation.value.dx, _animation.value.dy, 0.0),
                child: ZToBoxAdapter(
                    width: widget.boxWidth - widget.drawerStroke,
                    height: widget.boxHeight - widget.drawerStroke,
                    child: Material(
                      color: Colors.white,
                      child: InkWell(
                          onTap: () {
                            if (widget.drawerOnTap != null) {
                              widget.drawerOnTap();
                            }
                            if (!_isOpened) {
                              animationForward();
                            } else if (_isOpened) {
                              animationReverse();
                            }
                          },
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(widget.title,
                                      style: GoogleFonts.sourceCodePro(
                                          color: Colors.black,
                                          fontSize: 18))))),
                    ))),
            if (widget.bottomContent != null)
              ZPositioned(
                  translate: ZVector(0.0,
                      _bottomContentHeight / 2 + widget.boxHeight / 2, 0.0),
                  child: ZToBoxAdapter(
                      width: widget.boxWidth + 1.0,
                      height: _bottomContentHeight,
                      child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(8.0),
                                  bottomRight: Radius.circular(8.0))),
                          child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(8.0),
                                  bottomRight: Radius.circular(8.0)),
                              child: widget.bottomContent)))),
          ]);
        });
  }

  animationReverse() {
    _controller.reverse();
    _isOpened = false;
  }

  animationForward() {
    _controller.forward();
    _isOpened = true;
  }
}

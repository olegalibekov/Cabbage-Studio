import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:cabbage_studio/widgets/projects_box_portfolio.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart' hide Axis;

import 'menu_page.dart';

class ProjectDetailedPage extends StatefulWidget {
  final Project project;

  const ProjectDetailedPage(this.project, {Key key}) : super(key: key);

  @override
  _ProjectDetailedPageState createState() =>
      _ProjectDetailedPageState();
}

class _ProjectDetailedPageState extends State<ProjectDetailedPage>
    with TickerProviderStateMixin {
  List<ShapeData> _shapeList = [];
  final _listViewWidth = 400.0;

  @override
  void initState() {
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));
    _shapeList
        .add(createShape(this, Pyramid(200.0, 200.0, 3, 2, false), 200.0));
    _shapeList.add(createShape(this, Pyramid(50.0, 50.0, 3, 2, false), 50.0));
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));

    setShapesVelocity(_shapeList);

    super.initState();
  }

  @override
  void dispose() {
    disposeControllers(_shapeList);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _pageSize = MediaQuery.of(context).size;
    _shapeList[0].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.05, 0, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[1].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.8,
            -_pageSize.height / 2 * 0.6, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[2].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.95,
            -_pageSize.height / 2 * 0.4, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[3].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.9,
            _pageSize.height / 2 * 0.8, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    return Scaffold(
        body: Stack(alignment: Alignment.center, children: [
      ZIllustration(children: [
        ZGroup(sortMode: SortMode.stack, children: [
          for (var shapeIndex = 0; shapeIndex < _shapeList.length; shapeIndex++)
            AnimatedBuilder(
                animation: Listenable.merge([
                  _shapeList[shapeIndex].translationController,
                  _shapeList[shapeIndex].rotationController
                ]),
                builder: (context, child) {
                  if (_shapeList[shapeIndex].translationAnimation?.value !=
                      null) {
                    _shapeList[shapeIndex].offsetAnimation =
                        _shapeList[shapeIndex].translationAnimation.value;
                  }
                  return ZPositioned(
                      translate:
                          _shapeList[shapeIndex].startedZTransform.translate,
                      rotate: _shapeList[shapeIndex].startedZTransform.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            translate: ZVector(
                                _shapeList[shapeIndex].offsetAnimation.dx,
                                _shapeList[shapeIndex].offsetAnimation.dy,
                                0),
                            rotate: ZVector(
                                _shapeList[shapeIndex].currentRotation, 0, 0),
                            child: ZGroup(children: [
                              ZPositioned(
                                  translate: ZVector(
                                      0,
                                      _shapeList[shapeIndex].shapeHeight / 3,
                                      0),
                                  child: _shapeList[shapeIndex]
                                      .shape
                                      .withEdgesOpacity(
                                          shapeIndex != 1 ? 1.0 : 0.2))
                            ]))
                      ]));
                })
        ])
      ]),
      Positioned(
          left: 64.0,
          bottom: 24.0,
          child: bottomTitle(context, ': Ваши проекты')),
      ListView(scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
        Align(
            alignment: Alignment.topLeft,
            child: Padding(
                padding: const EdgeInsets.only(top: 108.0),
                child: Stack(alignment: Alignment.centerLeft, children: [
                  Container(
                      // height: 36.0,
                      // width: 135,
                      padding: EdgeInsets.zero,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 1.5),
                          borderRadius: BorderRadius.circular(4.0)),
                      child: Row(children: [
                        Padding(
                            padding:
                                const EdgeInsets.only(top: 4.5, right: 14.0),
                            child: Container(
                                width: 9.0,
                                height: 9.0,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    shape: BoxShape.circle))),
                        Text(widget?.project?.name?.value,
                            style: GoogleFonts.sourceCodePro(
                                fontSize: 28, color: Colors.black))
                      ]))
                ]))),
        SizedBox(width: 48),
        Align(
          child: SizedBox(
              width: _listViewWidth,
              height: MediaQuery.of(context).size.height,
              child: ListView(physics: BouncingScrollPhysics(), children: [
                SizedBox(height: 108.0),
                CachedNetworkImage(
                    imageUrl: widget?.project?.photoUrl?.value ?? '',
                    fit: BoxFit.cover,
                    height: 200.0,
                    alignment: Alignment.center),
                // SizedBox(height: 16.0),
                Markdown(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    imageBuilder: (uri, _, __) {
                      return SizedBox(
                        width: double.infinity,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                              child: CachedNetworkImage(
                                alignment: Alignment.centerLeft,
                                imageUrl: '$uri',
                                // fit: BoxFit.cover,
                                // height: 200.0,
                              )),
                        ),
                      );
                    },
                    data: widget?.project?.mdDescription?.value ?? '',
                    styleSheet: MarkdownStyleSheet.largeFromTheme(
                            Theme.of(context).copyWith(
                                textTheme: Theme.of(context).textTheme.apply(
                                    bodyColor: Colors.black,
                                    displayColor: Colors.black)))
                        .merge(MarkdownStyleSheet(blockSpacing: 16.0))),
                // styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context))
                //     .copyWith(em: TextStyle(color: Colors.black,fontSize: 30))),
                // Text(
                //     'Это началось очень давно. В этот проект вложили силы достаточно людей. Даже Майл!',
                //     style: GoogleFonts.sourceCodePro(
                //         color: Colors.black,
                //         fontWeight: FontWeight.w300,
                //         fontSize: 18)),
                // SizedBox(height: 16.0),
                // Align(
                //     alignment: Alignment.centerLeft,
                //     child: SizedBox(
                //         width: 150.0,
                //         height: 150.0,
                //         child: Container(
                //             decoration: BoxDecoration(
                //                 color: Colors.purple,
                //                 // border: Border.all(color: Colors.black),
                //                 borderRadius: BorderRadius.circular(8.0))))),
                SizedBox(height: 108.0)
              ])),
        ),
        SizedBox(width: _listViewWidth / 2)
      ]),
      Positioned(
          top: 24.0,
          left: 52.0,
          child: byCabbageStudio(true, () {
            goToPage(context, this.widget, MenuPage());
          }))
    ]));
  }
}

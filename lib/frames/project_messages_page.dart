import 'dart:async';
import 'dart:math';

import 'package:bubble/bubble.dart';
import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/widgets/empty_containers.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import 'menu_page.dart';

class ProjectMessagesPage extends StatefulWidget {
  final Project project;
  final bool isUser;

  const ProjectMessagesPage(this.project, this.isUser, {Key key})
      : super(key: key);

  @override
  _ProjectMessagesPageState createState() => _ProjectMessagesPageState();
}

class _ProjectMessagesPageState extends State<ProjectMessagesPage>
    with TickerProviderStateMixin {
  List<ShapeData> _shapeList = [];

  TextEditingController _messageController = TextEditingController();

  final _wholeCenterBlockSize = Size(800, 600);
  var _chat;
  Stream _chatStream;
  StreamController _streamController = StreamController();

  @override
  void initState() {
    _chat = db.root.chats[widget.project.chat.value].messages;
    // _chat = widget.project.chat;
    _chatStream = _chat.stream;
    _streamController.addStream(_chatStream);

    _shapeList
        .add(createShape(this, Pyramid(100.0, 100.0, 3, 2, false), 100.0));
    _shapeList
        .add(createShape(this, Pyramid(200.0, 200.0, 3, 2, false), 200.0));
    _shapeList.add(createShape(this, Pyramid(50.0, 50.0, 3, 2, false), 50.0));
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));

    setShapesVelocity(_shapeList);

    super.initState();
  }

  String messageAlignment(message) {
    return widget?.isUser == true && message?.isUser?.value == true
        ? 'Right'
        : widget?.isUser == true && message?.isUser?.value == false
            ? 'Left'
            : widget?.isUser == false && message?.isUser?.value == true
                ? 'Left'
                : widget?.isUser == false && message?.isUser?.value == false
                    ? 'Right'
                    : 'Center';
  }

  @override
  void dispose() {
    super.dispose();
    // _chatStream.drain();
    // _chatStream.distinct();
    _streamController.close();

    disposeControllers(_shapeList);
  }

  @override
  Widget build(BuildContext context) {
    var _pageSize = MediaQuery.of(context).size;
    _shapeList[0].startedZTransform = ZTransform(
        translate:
            ZVector(-_pageSize.width / 2 + _pageSize.width * 0.01, 100, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[1].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.8,
            -_pageSize.height / 2 * 0.6, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[2].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.95,
            -_pageSize.height / 2 * 0.4, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[3].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.9,
            _pageSize.height / 2 * 0.8, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    return Scaffold(
        body: Stack(children: [
      ZIllustration(children: [
        ZGroup(sortMode: SortMode.stack, children: [
          for (var shapeIndex = 0; shapeIndex < _shapeList.length; shapeIndex++)
            AnimatedBuilder(
                animation: Listenable.merge([
                  _shapeList[shapeIndex].translationController,
                  _shapeList[shapeIndex].rotationController
                ]),
                builder: (context, child) {
                  if (_shapeList[shapeIndex].translationAnimation?.value !=
                      null) {
                    _shapeList[shapeIndex].offsetAnimation =
                        _shapeList[shapeIndex].translationAnimation.value;
                  }
                  return ZPositioned(
                      translate:
                          _shapeList[shapeIndex].startedZTransform.translate,
                      rotate: _shapeList[shapeIndex].startedZTransform.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            translate: ZVector(
                                _shapeList[shapeIndex].offsetAnimation.dx,
                                _shapeList[shapeIndex].offsetAnimation.dy,
                                0),
                            rotate: ZVector(
                                _shapeList[shapeIndex].currentRotation, 0, 0),
                            child: ZGroup(children: [
                              ZPositioned(
                                  translate: ZVector(
                                      0,
                                      _shapeList[shapeIndex].shapeHeight / 3,
                                      0),
                                  child: _shapeList[shapeIndex]
                                      .shape
                                      .withEdgesOpacity(
                                          shapeIndex != 1 ? 1.0 : 0.2))
                            ]))
                      ]));
                })
        ]),
        ZPositioned(
            translate: ZVector(
                -_pageSize.width / 2 + 200, -_pageSize.height / 2 + 200, 0),
            rotate: ZVector(-0.15, 0.1, -0.45),
            child: bottomBox(235, 65, 235)),
        ZPositioned(
            translate: ZVector(
                -_pageSize.width / 2 + 225, _pageSize.height / 2 - 250, 0),
            rotate: ZVector(-0.03, 0.115, 0.35),
            child: topBox(130, 185, 235))
      ]),
      Positioned(
          top: 24.0,
          left: 52.0,
          child: byCabbageStudio(true, () {
            goToPage(context, this.widget, MenuPage());
            // Navigator.of(context).pop();
          })),
      Positioned(
          left: 40.0,
          bottom: 24.0,
          child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
            Text(': ${widget.project.name.value}',
                style: TextStyle(
                    fontFamily: 'SourceCodePro',
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w500)),
            SizedBox(width: 12.0),
            kIsWeb
                ? Image.network('assets/icons/right-arrow.svg',
                    width: 20.0, height: 20.0)
                : SvgPicture.asset('assets/icons/right-arrow.svg',
                    width: 20.0, height: 20.0),
            // SvgPicture.asset('assets/icons/right-arrow.svg',
            //     width: 20.0, height: 20.0),
            SizedBox(width: 12.0),
            Text('сообщения',
                style: TextStyle(
                    fontFamily: 'SourceCodePro',
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w300))
          ])),
      Positioned(
          left: _pageSize.width / 2 - _wholeCenterBlockSize.width / 2,
          top: _pageSize.height / 2 - _wholeCenterBlockSize.height / 2,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Text('Ключевые точки',
                //     style: GoogleFonts.sourceCodePro(
                //         // fontFamily: 'SourceCodePro',
                //         color: Colors.black,
                //         fontSize: 22,
                //         fontWeight: FontWeight.w300)),
                // SizedBox(height: 16.0),
                // Flexible(
                //     flex: 1,
                //     child: Container(
                //         color: Colors.black.withOpacity(0.03),
                //         child: ListView(
                //             // shrinkWrap: true,
                //             padding: const EdgeInsets.only(
                //                 left: 16.0,
                //                 top: 14.0,
                //                 bottom: 14.0,
                //                 right: 36.0),
                //             physics: BouncingScrollPhysics(),
                //             children: [
                //               listTitle('Содержание'),
                //               listTitle(
                //                   'Введение: Научимся использовать'),
                //               listTitle('Задание 1: Создание каталогов'),
                //               listSubTitle('Иерархия файлов и каталогов'),
                //               listTitle('Задание 2: Изменение прав'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //               listSubTitle('root: root 2'),
                //             ]))),
                SizedBox(
                    height: _wholeCenterBlockSize.height - 0.0,
                    width: _wholeCenterBlockSize.width - 0.0,
                    child: Padding(
                        padding: const EdgeInsets.only(
                            left: 30.0, top: 30.0, right: 30.0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StreamBuilder(
                                  // stream: _chatStream,
                                  stream: _streamController.stream,
                                  builder: (context, snapshot) {
                                    print('StreamBuilder update');
                                    var messages = _chat?.values ?? [];

                                    print(messages);
                                    var messagesUI = [];
                                    if (messages != [])
                                      for (var message in messages) {
                                        if (message?.isUser?.value != null &&
                                            message?.title?.value != null) {
                                          var side = messageAlignment(message);
                                          messagesUI.add(Align(
                                              alignment: side == 'Left'
                                                  ? Alignment.centerLeft
                                                  : side == 'Right'
                                                      ? Alignment.centerRight
                                                      : Alignment.center,
                                              child: Column(
                                                  crossAxisAlignment: side ==
                                                          'Left'
                                                      ? CrossAxisAlignment.start
                                                      : side == 'Right'
                                                          ? CrossAxisAlignment
                                                              .end
                                                          : CrossAxisAlignment
                                                              .center,
                                                  children: [
                                                    Container(
                                                        color: Colors.black
                                                            .withOpacity(0.2),
                                                        child: Padding(
                                                            padding: const EdgeInsets.symmetric(
                                                                vertical: 8.0),
                                                            child: Text(
                                                                message?.timestamp?.value != null
                                                                    ? message
                                                                        ?.timestamp
                                                                        ?.value
                                                                        ?.toString()
                                                                    : '',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black
                                                                        .withOpacity(
                                                                            0.6),
                                                                    fontSize:
                                                                        24.0,
                                                                    fontWeight:
                                                                        FontWeight.bold)))),
                                                    Padding(
                                                        padding: const EdgeInsets
                                                                .symmetric(
                                                            vertical: 12.0),
                                                        child: Bubble(
                                                            nip: side == 'Right'
                                                                ? BubbleNip
                                                                    .rightTop
                                                                : side == 'Left'
                                                                    ? BubbleNip
                                                                        .leftTop
                                                                    : BubbleNip
                                                                        .no,
                                                            color: side ==
                                                                    'Right'
                                                                ? Color
                                                                    .fromRGBO(
                                                                        225,
                                                                        255,
                                                                        199,
                                                                        1.0)
                                                                : Colors.white,
                                                            child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .all(
                                                                        4.0),
                                                                child: Text(
                                                                  message.title
                                                                      .value,
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          18.0),
                                                                ))))
                                                  ]))); // Align(

                                        }
                                      }

                                    return Expanded(
                                        child: ListView(
                                            padding: const EdgeInsets.only(
                                                top: 16.0, bottom: 16.0),
                                            physics: BouncingScrollPhysics(),
                                            children: [...messagesUI]));
                                  })
                            ]))),
                // Flexible(flex: 1, child: Container()),
                SizedBox(
                    width: _wholeCenterBlockSize.width,
                    child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Row(children: [
                          Expanded(
                              child: TextField(
                                  controller: _messageController,
                                  decoration:
                                      InputDecoration(hintText: 'Сообщение'),
                                  onChanged: (text) {
                                    setState(() {});
                                  })),
                          SizedBox(width: 12.0),
                          IconButton(
                              padding: const EdgeInsets.all(16.0),
                              visualDensity: VisualDensity.compact,
                              onPressed: _messageController.text != ''
                                  ? () async {
                                      var allChats = db.root.chats;
                                      var projectChatKey = widget.project.chat;

                                      if (projectChatKey?.value == 'null') {
                                        var createdChat = allChats.push;
                                        await createdChat.theme.set('Theme');

                                        projectChatKey.set(createdChat.key);
                                      }

                                      var message =
                                          allChats[projectChatKey.value]
                                              .messages
                                              .push;
                                      message.title = _messageController.text;
                                      message.isUser = widget.isUser;

                                      // print(DateTime.fromMillisecondsSinceEpoch(DateTime.now().millisecondsSinceEpoch));
                                      // DateTime.now().millisecondsSinceEpoch)

                                      message.timestamp =
                                          DateTime.now().millisecondsSinceEpoch;
                                      _messageController.clear();
                                    }
                                  : null,
                              icon: Icon(
                                Icons.send,
                              ))
                        ])))
              ]))
    ]));
  }

  listTitle(title) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Flexible(
              child: Text(title,
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                  overflow: TextOverflow.ellipsis))
        ]));
  }

  listSubTitle(title) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 16.0, left: 20.0),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Flexible(
              child: Text(title,
                  style: TextStyle(
                      fontFamily: 'SourceCodePro',
                      color: Colors.black.withOpacity(0.6),
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500),
                  overflow: TextOverflow.ellipsis))
        ]));
  }

  // Align(
  // alignment: side == 'Left'
  // ? Alignment.centerLeft
  //     : side == 'Right'
  // ? Alignment.centerRight
  //     : Alignment.center,
  // child: Padding(
  // padding: const EdgeInsets.all(8.0),
  // child: Bubble(
  // nip: side == 'Right'
  // ? BubbleNip.rightTop
  //     : side == 'Left'
  // ? BubbleNip.leftTop
  //     : BubbleNip.no,
  // color: side == 'Right'
  // ? Color.fromRGBO(225, 255, 199, 1.0)
  //     : Colors.white,
  // child: Text(message.title.value))))

  topBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Widget content) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Center(child: Stack(children: [emptyContainer(), content])));
    }

    return ZGroup(children: [
      ZPositioned(
          translate: ZVector(-width / 2 + 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(width / 2 - 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2 + 1.0),
          child: boxContainer(width, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2 - 1.0),
          child: boxContainer(width, height, Container(color: Colors.cyan))),
      ZPositioned(
          translate: ZVector(0, -height / 2 + 0.5, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: ZToBoxAdapter(
              width: width,
              height: depth,
              child: Container(color: Colors.white))),
      ZBox(
          height: height,
          width: width,
          depth: depth,
          color: Colors.black,
          fill: false)
    ]);
  }

  bottomBoxTitle(String title) {
    return Container(
        color: Colors.white,
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 14.0),
                child: Text(title,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w400)))));
  }

  bottomBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Widget content) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Stack(children: [
            emptyContainer(),
            Padding(padding: const EdgeInsets.all(1.0), child: content)
          ]));
    }

    return ZGroup(children: [
      ZPositioned(
          translate: ZVector(-width / 2 + 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(width / 2 - 1.0, 0, 0),
          rotate: ZVector(0, -pi / 2, 0),
          child: boxContainer(depth, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2 + 1.0),
          rotate: ZVector(0, -pi, 0),
          child: boxContainer(width, height, emptyContainer())),
      ZPositioned(
          translate: ZVector(0, -height / 2 + 1.0, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, emptyContainer())),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2 - 1.0),
          child: boxContainer(
              width, height, bottomBoxTitle('${widget.project.name.value}'))),
      ZBox(
          height: height,
          width: width,
          depth: depth,
          color: Colors.black,
          fill: false)
    ]);
  }
}

import 'package:flutter/material.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

class HeroContentPage extends StatefulWidget {
  final content;

  const HeroContentPage(this.content, {Key key}) : super(key: key);

  @override
  _HeroContentPageState createState() => _HeroContentPageState();
}

class _HeroContentPageState extends State<HeroContentPage> {
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Hero(tag: 'heroAnimation', child: widget.content),
      Positioned(
          left: 16.0,
          top: 16.0,
          child: PointerInterceptor(
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  })))
    ]));
  }
}

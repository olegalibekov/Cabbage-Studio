import 'package:cabbage_studio/frames/menu_page.dart';
import 'package:cabbage_studio/frames/project_messages_page.dart';
import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:cabbage_studio/widgets/projects_box_portfolio.dart';
import 'package:cabbage_studio/widgets/projects_box_users.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';

import '../main.dart';

class AllProjectMessages extends StatefulWidget {
  @override
  _AllProjectMessagesState createState() => _AllProjectMessagesState();
}

class _AllProjectMessagesState extends State<AllProjectMessages>
    with TickerProviderStateMixin {
  List<ShapeData> _shapeList = [];

  @override
  void initState() {
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));
    _shapeList
        .add(createShape(this, Pyramid(200.0, 200.0, 3, 2, false), 200.0));
    _shapeList.add(createShape(this, Pyramid(50.0, 50.0, 3, 2, false), 50.0));
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));

    setShapesVelocity(_shapeList);

    super.initState();
  }

  @override
  void dispose() {
    disposeControllers(_shapeList);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _pageSize = MediaQuery.of(context).size;
    _shapeList[0].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.05, 0, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[1].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.8,
            -_pageSize.height / 2 * 0.6, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[2].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.95,
            -_pageSize.height / 2 * 0.4, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[3].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.9,
            _pageSize.height / 2 * 0.8, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    return Scaffold(
        body: Stack(children: [
          ZIllustration(children: [
            ZGroup(sortMode: SortMode.stack, children: [
              for (var shapeIndex = 0;
              shapeIndex < _shapeList.length;
              shapeIndex++)
                AnimatedBuilder(
                    animation: Listenable.merge([
                      _shapeList[shapeIndex].translationController,
                      _shapeList[shapeIndex].rotationController
                    ]),
                    builder: (context, child) {
                      if (_shapeList[shapeIndex]
                          .translationAnimation
                          ?.value !=
                          null) {
                        _shapeList[shapeIndex].offsetAnimation =
                            _shapeList[shapeIndex]
                                .translationAnimation
                                .value;
                      }
                      return ZPositioned(
                          translate: _shapeList[shapeIndex]
                              .startedZTransform
                              .translate,
                          rotate: _shapeList[shapeIndex]
                              .startedZTransform
                              .rotate,
                          child: ZGroup(children: [
                            ZPositioned(
                                translate: ZVector(
                                    _shapeList[shapeIndex]
                                        .offsetAnimation
                                        .dx,
                                    _shapeList[shapeIndex]
                                        .offsetAnimation
                                        .dy,
                                    0),
                                rotate: ZVector(
                                    _shapeList[shapeIndex]
                                        .currentRotation,
                                    0,
                                    0),
                                child: ZGroup(children: [
                                  ZPositioned(
                                      translate: ZVector(
                                          0,
                                          _shapeList[shapeIndex]
                                              .shapeHeight /
                                              3,
                                          0),
                                      child: _shapeList[shapeIndex]
                                          .shape
                                          .withEdgesOpacity(
                                          shapeIndex != 1
                                              ? 1.0
                                              : 0.2))
                                ]))
                          ]));
                    })
            ])
          ]),
          Center(
              child: SizedBox(
                  width: 200,
                  child: StreamBuilder(
                      stream:
                      db.root.users[currentUser].userProjects.stream,
                      builder: (context, snapshot) {
                        var projects = snapshot?.data?.values ?? [];
                        // print(projects);
                        var allProjectsVisually = [];

                        projects.forEach((project) {
                          allProjectsVisually.add(Padding(

                            padding: const EdgeInsets.symmetric(vertical:8.0),
                            child: FlatButton(
                                onPressed: () {
                                  goToPage(context, this.widget,
                                      ProjectMessagesPage(project, isUser));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('${project.name.value}',style: TextStyle(fontSize: 24.0),),
                                )),
                          ));
                        });
                        return ListView(
                            shrinkWrap: true,
                            children: [...allProjectsVisually]);
                      }))),
          Positioned(
              top: 24.0,
              left: 52.0,
              child: byCabbageStudio(true, () {
                // goToPage(context, this.widget, MenuPage());
                Navigator.push(context, MaterialPageRoute(builder: (context) => MenuPage())).then((value) => setState((){}));
              })),
          Positioned(
              left: 64.0,
              bottom: 24.0,
              child: bottomTitle(context, ': Ваши сообщения')),
        ]));
  }
}

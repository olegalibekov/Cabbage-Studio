import 'dart:math';

import 'package:cabbage_studio/frames/menu_page.dart';
import 'package:cabbage_studio/shapes/animated_shape.dart';
import 'package:cabbage_studio/widgets/drawer_widget.dart';
import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:zflutter/zflutter.dart' hide Axis;

class PersonalAccountPage extends StatefulWidget {
  @override
  _PersonalAccountPageState createState() => _PersonalAccountPageState();
}

class _PersonalAccountPageState extends State<PersonalAccountPage>
    with TickerProviderStateMixin {
  List<ShapeData> _shapeList = [];

  @override
  void initState() {
    _shapeList
        .add(createShape(this, Pyramid(200.0, 200.0, 3, 2, false), 200.0));
    _shapeList.add(createShape(this, Pyramid(50.0, 50.0, 3, 2, false), 50.0));
    _shapeList
        .add(createShape(this, Pyramid(150.0, 150.0, 3, 2, false), 150.0));

    setShapesVelocity(_shapeList);

    super.initState();
  }

  @override
  void dispose() {
    disposeControllers(_shapeList);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _pageSize = MediaQuery.of(context).size;
    _shapeList[0].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.8,
            -_pageSize.height / 2 * 0.6, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[1].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.95,
            -_pageSize.height / 2 * 0.4, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    _shapeList[2].startedZTransform = ZTransform(
        translate: ZVector(-_pageSize.width / 2 + _pageSize.width * 0.9,
            _pageSize.height / 2 * 0.8, 0),
        rotate: ZVector(-0.08, -0.6, -0.45));
    return Scaffold(
        body: Stack(alignment: Alignment.center, children: [
      ZIllustration(children: [
        ZGroup(sortMode: SortMode.stack, children: [
          for (var shapeIndex = 0; shapeIndex < _shapeList.length; shapeIndex++)
            AnimatedBuilder(
                animation: Listenable.merge([
                  _shapeList[shapeIndex].translationController,
                  _shapeList[shapeIndex].rotationController
                ]),
                builder: (context, child) {
                  if (_shapeList[shapeIndex].translationAnimation?.value !=
                      null) {
                    _shapeList[shapeIndex].offsetAnimation =
                        _shapeList[shapeIndex].translationAnimation.value;
                  }
                  return ZPositioned(
                      translate:
                          _shapeList[shapeIndex].startedZTransform.translate,
                      rotate: _shapeList[shapeIndex].startedZTransform.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            translate: ZVector(
                                _shapeList[shapeIndex].offsetAnimation.dx,
                                _shapeList[shapeIndex].offsetAnimation.dy,
                                0),
                            rotate: ZVector(
                                _shapeList[shapeIndex].currentRotation, 0, 0),
                            child: ZGroup(children: [
                              ZPositioned(
                                  translate: ZVector(
                                      0,
                                      _shapeList[shapeIndex].shapeHeight / 3,
                                      0),
                                  child: _shapeList[shapeIndex]
                                      .shape
                                      .withEdgesOpacity(
                                          shapeIndex != 1 ? 1.0 : 0.2))
                            ]))
                      ]));
                }),
        ]),
        ZPositioned(
            translate: ZVector(0.0, 225.0, 0.0),
            rotate: ZVector.only(x: pi / 2),
            child: ZPositioned(
                rotate: ZVector(-0.40, 0, 0.0),
                // rotate: controller.rotate,
                child: meditationPlace(275, 250))),
        // ZPositioned(
        //     translate: ZVector(-10, 225.0 - 200 / 2, 0.0),
        //     child: ZToBoxAdapter(
        //         width: 150, height: 200, child: Container(color: Colors.purple))),
        ZPositioned(
            translate: ZVector(400.0, 0.0, 0.0),
            rotate: ZVector(-0.125, 0.115, 0),
            child: ZGroup(children: [
              ZPositioned(
                  // rotate: controller.rotate,
                  child: DrawerWidget(170, 350, 200, true))
            ]))
      ]),
      Positioned(
          left: _pageSize.width / 2 - 500,
          top: _pageSize.height / 2 + 125,
          child: Text('здесь сундук с\nкапустой',
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w500,
                  fontSize: 22.0))),
      Positioned(
          left: 64.0,
          bottom: 24.0,
          child: bottomTitle(context, ': Личный кабинет')),
      Positioned(
          top: 24.0,
          left: 52.0,
          child: byCabbageStudio(
              true,(){ goToPage(context, this.widget, MenuPage());}))
    ]));
  }

  meditationPlace(double width, double height) {
    final backLeftPadding = width * 0.06;
    final backRightPadding = width * 0.14;

    final backLeftLineWidth = width * 0.1;
    final backRightLineWidth = width * 0.075;

    return ZShape(path: [
      // ZMove(-width / 2 + backLeftLineWidth + backLeftPadding, -height / 2, 0.0),
      ZMove(-width / 2 + backLeftPadding, -height / 2, 0.0),
      ZLine(-width / 2, height / 2, 0.0),
      ZLine(width / 2, height / 2, 0.0),
      ZLine(width / 2 - backRightPadding, -height / 2, 0.0),
      // ZLine(
      //     width / 2 - backRightLineWidth - backRightPadding, -height / 2, 0.0),
    ], closed: true, color: Colors.black, stroke: 1.5);
  }
}

import 'dart:math';

import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:cabbage_studio/widgets/pages_templates.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart';

extremeVolume(List<ShapeData> shapeList, bool max) {
  return shapeList
      .reduce((curr, next) {
        // print(curr.shapeVolume);
        if (max) {
          return curr.shapeVolume > next.shapeVolume ? curr : next;
        } else {
          return curr.shapeVolume < next.shapeVolume ? curr : next;
        }
      })
      .shapeVolume
      .toDouble();
}

class Pyramid {
  final double baseRadius;
  final double height;
  final int faces;
  final double stroke;
  final bool withContainers;

  Pyramid(this.baseRadius, this.height, this.faces, this.stroke,
      this.withContainers) {
    initPointsAndPaths();
  }

  pyramidVolume() {
    /// Taken from https://www.resolventa.ru/spr/planimetry/regular.htm#reg5
    // double baseEdgeLength = 2 * radius * sin(pi / faces);
    // double basePerimeter = 2 * radius * faces * sin(pi / faces);
    double baseSquare =
        1 / 2 * pow(baseRadius, 2) * faces * sin(2 * pi / faces);
    double volume = 1 / 3 * baseSquare * height;
    return volume;
  }

  List<ZVector> pyramidBase(double radius, int faces) {
    bool pathBeginning = true;
    List<ZVector> pyramidBasePath = [];

    List<int> xPoints = [];
    List<int> yPoints = [];

    double x = 0;
    double y = 0;
    double R = radius;
    int k = faces;
    double angle = 0;
    int i = 0;
    k = 360 ~/ k;

    while (angle <= 360 + k) {
      i = i + 1;
      xPoints.add((x + cos(angle / 180 * pi) * R).round());
      yPoints.add((y - sin(angle / 180 * pi) * R).round());
      angle = angle + k;
    }

    while (i - 2 > 0) {
      i = i - 1;
      if (pathBeginning) {
        pyramidBasePath
            .add(ZVector(xPoints[i].toDouble(), 0.0, yPoints[i].toDouble()));
        pyramidBasePath.add(
            ZVector(xPoints[i - 1].toDouble(), 0.0, yPoints[i - 1].toDouble()));
        pathBeginning = false;
      } else {
        pyramidBasePath.add(
            ZVector(xPoints[i - 1].toDouble(), 0.0, yPoints[i - 1].toDouble()));
      }
    }

    return pyramidBasePath;
  }

  double distance(ZVector pointA, ZVector pointB) {
    return sqrt(pow((pointB.x - pointA.x), 2) +
        pow((pointB.y - pointA.y), 2) +
        pow((pointB.z - pointA.z), 2));
  }

  List<ZVector> pyramidBasePoints;
  ZVector topPointPosition;
  List<ZPathCommand> pyramidBottomZPath = [];
  List<ZPathCommand> pyramidTopZPath = [];

  initPointsAndPaths() {
    pyramidBasePoints = pyramidBase(baseRadius, faces);
    topPointPosition = ZVector(0.0, -height.toDouble(), 0.0);

    pyramidBottomZPath.add(ZMove.vector(pyramidBasePoints[0]));
    for (int bottomPoint = 1;
        bottomPoint < pyramidBasePoints.length;
        bottomPoint++)
      pyramidBottomZPath.add(ZLine.vector(pyramidBasePoints[bottomPoint]));

    for (int bottomPoint = 1;
        bottomPoint < pyramidBasePoints.length;
        bottomPoint++) {
      pyramidTopZPath.add(ZMove.vector(topPointPosition));
      pyramidTopZPath.add(ZLine.vector(pyramidBasePoints[bottomPoint]));
    }
  }

  createPyramidShape(double opacity) {
    return ZShape(
        path: [...pyramidBottomZPath, ...pyramidTopZPath],
        color: Colors.black.withOpacity(opacity),
        stroke: stroke);
  }

  createPyramidContainers() {
    List<ZPositioned> containersPosition = [];
    double pointsDistanceFirst =
        distance(pyramidBasePoints[0], pyramidBasePoints[1]) / 2;

    double pointsDistanceSecond =
        distance(ZVector(0, 0, 0), pyramidBasePoints[1]);

    double cathetus =
        sqrt(pow(pointsDistanceSecond, 2) - pow(pointsDistanceFirst, 2));

    double clipPathTriangleAngleInRadians =
        atan(cathetus / -topPointPosition.y);

    double clipPathTriangleWidth =
        distance(pyramidBasePoints[0], pyramidBasePoints[1]);
    double clipPathTriangleHeight = distance(
        ZVector((pyramidBasePoints[0].x + pyramidBasePoints[0 + 1].x) / 2, 0,
            (pyramidBasePoints[0].z + pyramidBasePoints[0 + 1].z) / 2),
        topPointPosition);
    Size clipPathTriangleSize =
        Size(clipPathTriangleWidth, clipPathTriangleHeight);

    double degreesToRads(double deg) => (deg * pi) / 180.0;

    final angleBetweenFaces = degreesToRads(360 / faces);
    Random _random = Random();

    for (int currentFace = 0; currentFace < faces; currentFace++) {
      int r = _random.nextInt(255);
      int g = _random.nextInt(255);
      int b = _random.nextInt(255);
      var visualFace = ZPositioned(
          translate: ZVector(
              (pyramidBasePoints[currentFace].x +
                      pyramidBasePoints[currentFace + 1].x) /
                  2,
              0,
              (pyramidBasePoints[currentFace].z +
                      pyramidBasePoints[currentFace + 1].z) /
                  2),
          rotate: ZVector(
              0, -pi / faces + pi / 2 + currentFace * angleBetweenFaces, 0.0),
          child: ZGroup(children: [
            ZPositioned(
                rotate: ZVector(-clipPathTriangleAngleInRadians, 0, 0),
                child: ZGroup(children: [
                  ZPositioned(
                      translate:
                          ZVector(0, -clipPathTriangleSize.height / 2, 0),
                      child: ZToBoxAdapter(
                          width: clipPathTriangleSize.width,
                          height: clipPathTriangleSize.height,
                          child: ClipPath(
                              child:
                                  Container(color: Color.fromRGBO(r, g, b, 1)),
                              // Container(color: Colors.white),
                              clipper: PyramidFaceClipper())))
                ]))
          ]));
      containersPosition.add(visualFace);
    }
    return containersPosition;
  }

  // ZWidget withEdgesOpacity(double opacity) {
  //   // print(pyramidBasePoints[0]);
  //   ZWidget pyramidWithContainers = ZPositioned(
  //       rotate: ZVector(0, pi / faces, 0),
  //       child: ZGroup(sortMode: SortMode.stack, children: [
  //         createPyramidShape(opacity),
  //         if (withContainers) ...createPyramidContainers()
  //       ]));
  //   return pyramidWithContainers;
  // }
  ZWidget withEdgesOpacity(double opacity) {
    // print(pyramidBasePoints[0]);
    ZWidget pyramidWithContainers = ZPositioned(
        rotate: ZVector(0, pi / faces, 0),
        child: ZGroup(sortMode: SortMode.stack, children: [
          createPyramidShape(opacity),
          if (withContainers) ...createPyramidContainers(),
        ]));
    return pyramidWithContainers;
  }

  ZWidget withEdgesOpacityAndTitles(double opacity,
      [Function buttonImplementation]) {
    ZWidget pyramidWithContainers = ZPositioned(
        rotate: ZVector(0, pi / faces, 0),
        child: ZGroup(sortMode: SortMode.stack, children: [
          createPyramidShape(opacity),
          if (withContainers) ...createPyramidContainers(),
          ZPositioned(
              translate: ZVector(
                  (pyramidBasePoints[1].x + pyramidBasePoints[2].x) / 2,
                  (pyramidBasePoints[1].y + pyramidBasePoints[2].y) / 2,
                  (pyramidBasePoints[1].z + pyramidBasePoints[2].z) / 2),
              rotate: ZVector(0, pi / faces, 0),
              child: ZGroup(children: [
                ZPositioned(
                    translate: ZVector(45, 0, 0),
                    rotate: ZVector(pi / 2, pi, 0.2),
                    child: ZToBoxAdapter(
                        width: 300,
                        height: 100,
                        child: Stack(children: [
                          Positioned(
                              top: 0,
                              left: 0,
                              child: Text('Welcome',
                                  style: GoogleFonts.spaceMono(
                                      fontSize: 64,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w800))),
                          SizedBox(height: 20),
                          Positioned(
                              bottom: 0,
                              left: 0,
                              child: RichText(
                                  text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                    text: 'lets explore ',
                                    style: GoogleFonts.sourceCodePro(
                                        // height: 0.1,
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300)),
                                TextSpan(
                                    text: 'something new',
                                    style: GoogleFonts.sourceCodePro(
                                        // height: 0.1,
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600))
                              ])))
                        ])))
              ])),
          ZPositioned(
              translate: ZVector(
                  (pyramidBasePoints[2].x + pyramidBasePoints[3].x) / 2,
                  (pyramidBasePoints[2].y + pyramidBasePoints[3].y) / 2,
                  (pyramidBasePoints[2].z + pyramidBasePoints[3].z) / 2),
              child: ZGroup(children: [
                ZPositioned(
                    translate: ZVector(-23, -5, 0),
                    rotate: ZVector(pi / 2 - 0.3, pi, -pi / 3.9),
                    child: ZToBoxAdapter(
                        width: 118.0,
                        height: 38.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(18.0),
                            color: Colors.white,
                            child:
                                byCabbageStudio(false, buttonImplementation))))
              ]))
        ]));
    return pyramidWithContainers;
  }
}

class PyramidFaceClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path()
      ..moveTo(size.width / 2, 0)
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}

import 'dart:math';

import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';

double globalVelocityMultiplication = 1.0;
final double maxVelocity = 0.10;
final double minVelocity = 0.03;

class ShapeData {
  final AnimationController translationController;
  final Animation translationAnimation;
  final Simulation translationSimulation;
  final Tween translationTween;
  final AnimationController accelerationController;
  final Animation accelerationAnimation;
  final Tween accelerationTween;
  final AnimationController rotationController;
  final shape;
  final shapeHeight;
  final shapeVolume;
  var startedZTransform;

  double shapeMaxVelocity = maxVelocity;

  double get neutralVelocity => shapeMaxVelocity / 6;

  /// [0..1]
  double shapeDecelerationQuotient = 0;
  double currentRotation = 0.0;
  double currentVelocity = 0.0;
  Offset offsetAnimation = Offset(0, 0);

  final Random random = Random();

  ShapeData(
      this.translationController,
      this.translationAnimation,
      this.translationSimulation,
      this.rotationController,
      this.accelerationController,
      this.accelerationAnimation,
      this.translationTween,
      this.accelerationTween,
      this.currentRotation,
      this.shape,
      this.shapeHeight,
      this.shapeVolume) {
    keepAcceleration();
  }

  runTranslation(Offset offsetBegin, Offset offsetEnd) {
    translationTween.begin = offsetBegin *
        (1 - shapeDecelerationQuotient) *
        globalVelocityMultiplication;
    translationTween.end = offsetEnd *
        (1 - shapeDecelerationQuotient) *
        globalVelocityMultiplication;
    translationController.drive(translationTween);
    translationController.animateWith(translationSimulation);
  }

  accelerationRotation(double acceleration) {
    accelerationTween.begin = currentVelocity;
    accelerationTween.end = acceleration;
    accelerationTween.animate(accelerationController);

    accelerationController.duration = Duration(milliseconds: 3000);

    accelerationController.addListener(() {
      currentVelocity = accelerationAnimation.value;
    });

    accelerationController
      ..reset()
      ..forward();
  }

  keepAcceleration() {
    rotationController.addStatusListener((status) {
      if (rotationController.status == AnimationStatus.completed) {
        currentRotation += currentVelocity * globalVelocityMultiplication;
        rotationController
          ..duration = Duration(milliseconds: 1)
          ..reset()
          ..forward();
      }
    });

    rotationController
      ..duration = Duration.zero
      ..reset()
      ..forward();
  }
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';

import 'pyramid.dart';
import 'shape_data.dart';

final Random _random = Random();

createShape(
    TickerProvider tickerProvider, Pyramid pyramid, double pyramidHeight) {
  Pyramid _pyramid = pyramid;
  AnimationController _pyramidTranslationController =
      AnimationController.unbounded(vsync: tickerProvider);
  const _pyramidSpring = SpringDescription(mass: 1, stiffness: 1, damping: 0);
  Simulation _pyramidTranslationSimulation =
      SpringSimulation(_pyramidSpring, 0, 1, 0);
  Tween _pyramidTween = Tween(begin: Offset(0, 0), end: Offset(0, 0));
  Animation _pyramidTranslationAnimation =
      _pyramidTranslationController.drive(_pyramidTween);
  AnimationController _pyramidAccelerationController =
      AnimationController(vsync: tickerProvider);
  AnimationController _pyramidRotationController =
      AnimationController(vsync: tickerProvider);

  /// Was Tween(begin: _currentVelocity, end: acceleration)
  Tween _pyramidAccelerationTween = Tween(begin: 0.0, end: 0.0);
  Animation _pyramidAccelerationAnimation =
      _pyramidAccelerationTween.animate(_pyramidAccelerationController);
  double _pyramidCurrentRotation = 0.0;

  ShapeData _pyramidShapeData = ShapeData(
      _pyramidTranslationController,
      _pyramidTranslationAnimation,
      _pyramidTranslationSimulation,
      _pyramidRotationController,
      _pyramidAccelerationController,
      _pyramidAccelerationAnimation,
      _pyramidTween,
      _pyramidAccelerationTween,
      _pyramidCurrentRotation,
      _pyramid,
      pyramidHeight,
      _pyramid.pyramidVolume());

  return _pyramidShapeData;
}

setShapesVelocity(List<ShapeData> shapeList) {
  double _shapeListMinVolume = extremeVolume(shapeList, false);
  double _shapeListMaxVolume = extremeVolume(shapeList, true);
  if (shapeList.length > 1) {
    for (int shapeIndex = 0; shapeIndex < shapeList.length; shapeIndex++) {
      var shape = shapeList[shapeIndex];

      double shapeDecelerationQuotient =
          (shape.shapeVolume - _shapeListMinVolume) /
              (_shapeListMaxVolume - _shapeListMinVolume);
      double shapeMaxVelocity = minVelocity +
          (1 - shapeDecelerationQuotient) * (maxVelocity - minVelocity);

      shape.shapeMaxVelocity = shapeMaxVelocity;
      shape.shapeDecelerationQuotient = shapeDecelerationQuotient;

      double _leftPyramidRandomX = _random.nextInt(90) - 45.0;
      double _leftPyramidRandomY = _random.nextInt(90) - 45.0;
      shape.runTranslation(shape.offsetAnimation,
          Offset(_leftPyramidRandomX, _leftPyramidRandomY));
      shape.accelerationRotation(
          _random.nextBool() ? shape.neutralVelocity : -shape.neutralVelocity);
    }
  }
  return shapeList;
}


disposeControllers(List<ShapeData> shapeList) {
  shapeList.forEach((element) {
    element.accelerationController.dispose();
    element.rotationController.dispose();
    element.translationController.dispose();
  });
}
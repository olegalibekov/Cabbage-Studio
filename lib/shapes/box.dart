import 'dart:math';

import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

class Box {

  parallelepiped(double width, double height) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Color boxColor) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Container(color: boxColor));
    }

    return ZGroup(children: [
      ZBox(
        height: height,
        width: width,
        depth: height,
        color: Colors.red,
        // frontColor: Color(0xffCC2255),
        // topColor: Colors.yellow,
        // leftColor: Colors.green,
        // rightColor: Colors.blue,
        // bottomColor: Colors.orange,
        // rearColor: Colors.red,
      ),
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(height, height, Colors.cyan)),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(height, height, Colors.teal)),
      ZPositioned(
          translate: ZVector(0, -height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, height, Colors.cyanAccent)),
      ZPositioned(
          translate: ZVector(0, height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, height, Colors.tealAccent)),
      ZPositioned(
          translate: ZVector(0, 0, -height / 2),
          child: boxContainer(width, height, Colors.blue)),
      ZPositioned(
          translate: ZVector(0, 0, height / 2),
          child: boxContainer(width, height, Colors.teal))
    ]);
  }
}

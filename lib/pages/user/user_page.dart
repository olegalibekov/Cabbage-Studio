import 'package:cabbage_studio/pages/user/user_news_page.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';
import '../project/project_card.dart';
import '../project/project_edit_page.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key key}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  var _nickController = TextEditingController();
  var _eMailController = TextEditingController();

  var _userId = 'UserId1';
  var _userFuture;

  @override
  void initState() {
    _userFuture = db.root.users[_userId].future;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Padding(
          padding: const EdgeInsets.all(24),
          child: CustomScrollView(slivers: [
            SliverToBoxAdapter(
                child: Text('Пользователь',
                    style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
            SliverToBoxAdapter(child: SizedBox(height: 12)),
            SliverToBoxAdapter(
                child: FutureBuilder(
                    future: _userFuture,
                    builder:
                        (BuildContext context, AsyncSnapshot<User> snapshot) {
                      var nick = snapshot.data?.nick?.value;
                      _nickController.text = (nick == 'null' ? '' : nick);
                      return TextField(
                          controller: _nickController,
                          decoration: InputDecoration(hintText: 'Nick'),
                          onChanged: (text) {
                            db.root.users[_userId].nick = text;
                          });
                    })),
            SliverToBoxAdapter(
                child: FutureBuilder(
                    future: _userFuture,
                    builder:
                        (BuildContext context, AsyncSnapshot<User> snapshot) {
                      var eMail = snapshot.data?.eMail?.value;
                      _eMailController.text = (eMail == 'null' ? '' : eMail);
                      return TextField(
                          controller: _eMailController,
                          decoration: InputDecoration(hintText: 'E-mail'),
                          onChanged: (text) {
                            db.root.users[_userId].eMail = text;
                          });
                    })),
            SliverToBoxAdapter(
                child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          EnterExitRoute(
                              exitPage: this.widget,
                              enterPage: UserNewsPage()));
                    },
                    child: Text('Новости',
                        style: TextStyle(color: Colors.black45)))),
            SliverToBoxAdapter(
                child: TextButton(
                    onPressed: () {
                      var newProject = db.root.users[_userId].userProjects.push;
                      Navigator.push(
                          context,
                          EnterExitRoute(
                              exitPage: this.widget,
                              enterPage: ProjectEditPage(newProject, true)));
                    },
                    child: Text('Создать проект',
                        style: TextStyle(color: Colors.black45)))),
            SliverToBoxAdapter(
                child: StreamBuilder(
                    stream: db.root.users[_userId].stream,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      var projects = snapshot.data?.userProjects?.values ?? [];
                      return ListView(
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                          children: [
                            for (var project in projects)
                              ProjectCard(project, this.widget, true)
                          ]);
                    }))
          ]),
        ));
  }
}

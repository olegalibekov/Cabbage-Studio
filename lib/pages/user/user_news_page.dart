import 'package:cabbage_studio/pages/news/news_card.dart';

import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';

import '../../frames/welcome_page.dart';

class UserNewsPage extends StatefulWidget {
  @override
  _UserNewsPageState createState() => _UserNewsPageState();
}

class _UserNewsPageState extends State<UserNewsPage> {
  var _news = db.root.globalinfo.news.stream;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Column(children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(
                          left: 16.0, top: 28.0, bottom: 4.0),
                      child: pageTitle('Новости'))),
              Expanded(
                  child: StreamBuilder(
                      stream: _news,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        var allNews = snapshot?.data?.values ?? [];

                        var allNewsVisual = [];
                        if (allNews != []) {
                          for (var news in allNews) {
                            allNewsVisual.add(Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 24, vertical: 8.0),
                              child: IgnorePointer(
                                  child: NewsCard(
                                      news..packet = true, this.widget)),
                            ));
                          }
                        }
                        return ListView(
                            physics: BouncingScrollPhysics(),
                            padding: const EdgeInsets.all(0.0),
                            children: [...allNewsVisual]);
                      }))
            ])));
  }
}

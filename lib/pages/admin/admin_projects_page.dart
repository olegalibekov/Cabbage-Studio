import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';
import '../../main.dart';
import '../project/project_card.dart';
import '../project/project_edit_page.dart';
import '../../frames/welcome_page.dart';

class ProjectsPage extends StatefulWidget {
  final bool isPortfolioProjects;

  const ProjectsPage(this.isPortfolioProjects, {Key key}) : super(key: key);

  @override
  _ProjectsPageState createState() => _ProjectsPageState();
}

class _ProjectsPageState extends State<ProjectsPage> {
  var _portfolio = db.root.globalinfo.projects.stream;
  var _users = db.root.users.stream;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(
          children: [
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    })),
            Center(
              child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: 600.0,
                  child: Column(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(
                                left: 16.0, top: 24.0, bottom: 8.0),
                            child: pageTitle(widget.isPortfolioProjects
                                ? 'Проекты портфолио'
                                : 'Проекты пользователей'))),
                    // Expanded(
                    //     child: FutureBuilder(
                    //         future: _allProjectsFuture,
                    //         builder: (BuildContext context, AsyncSnapshot snapshot) {
                    //           var projects = snapshot.data?.values ?? [];
                    //           return ListView(children: [
                    //             for (var project in projects)
                    //               ProjectCard(project: project)
                    //           ]);
                    //         })),
                    if (widget.isPortfolioProjects)
                      Expanded(
                          child: StreamBuilder(
                              stream: _portfolio,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                var projects = snapshot?.data?.values ?? [];

                                var allProjects = [];
                                if (projects != []) {
                                  for (var project in projects) {
                                    allProjects.add(Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 12.0),
                                        child: ProjectCard(
                                            project, this.widget, false)));
                                  }
                                }

                                return ListView(
                                    physics: BouncingScrollPhysics(),
                                    padding: const EdgeInsets.all(0.0),
                                    children: [...allProjects]);
                              })),
                    if (!widget.isPortfolioProjects)
                      Expanded(
                          child: StreamBuilder(
                              stream: _users,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                var users = snapshot?.data?.values ?? [];

                                var allProjects = [];
                                if (users != []) {
                                  for (var user in users) {
                                    for (var userProject
                                        in user?.userProjects?.values) {
                                      allProjects.add(Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 12.0),
                                          child: ProjectCard(userProject,
                                              this.widget, false)));
                                    }
                                  }
                                }

                                return ListView(
                                    physics: BouncingScrollPhysics(),
                                    padding: const EdgeInsets.all(0.0),
                                    children: [...allProjects]);
                              })),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FlatButton(
                            onPressed: () {
                              var newProject;
                              if (widget.isPortfolioProjects) {
                                newProject = db.root.globalinfo.projects.push;
                              } else {
                                newProject = db
                                    .root.users[currentUser].userProjects.push;
                              }

                              Navigator.push(
                                  context,
                                  EnterExitRoute(
                                      exitPage: this.widget,
                                      enterPage:
                                          ProjectEditPage(newProject, false)));
                            },
                            child: Text('Создать проект',
                                style: TextStyle(color: Colors.black45))))
                  ])),
            ),
          ],
        ));
  }
}

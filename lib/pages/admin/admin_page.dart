import 'package:cabbage_studio/pages/admin/admin_news_page.dart';
import 'package:cabbage_studio/pages/admin/admin_projects_page.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:flutter/material.dart';
import 'package:micro_utils/data_helper.dart';

class AdminPage extends StatelessWidget with AdminPageHelper {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          EnterExitRoute(
                              exitPage: AdminPage(), enterPage: AdminNewsPage()));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Новости',
                          style: TextStyle(color: Colors.black45, fontSize: 20)),
                    )),
                  TextButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            EnterExitRoute(
                                exitPage: AdminPage(),
                                enterPage: ProjectsPage(true)));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Проекты портфолио',
                            style:
                                TextStyle(color: Colors.black45, fontSize: 20)),
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            EnterExitRoute(
                                exitPage: AdminPage(),
                                enterPage: ProjectsPage(false)));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Проекты пользователей',
                            style:
                                TextStyle(color: Colors.black45, fontSize: 20)),
                      )),
                ],
              ),
            ),
            SizedBox(width: 12)
          ],
        )));
  }
}

@DataHelper()
class AdminPageHelper {}

import 'package:cabbage_studio/pages/news/news_card.dart';
import 'package:cabbage_studio/frames/welcome_page.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';

import '../project/project_card.dart';
import '../news/news_edit_page.dart';

class AdminNewsPage extends StatefulWidget {
  @override
  _AdminNewsPageState createState() => _AdminNewsPageState();
}

class _AdminNewsPageState extends State<AdminNewsPage> {
  var _news = db.root.globalinfo.news.stream;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(
          children: [
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    })),
            Center(
              child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: 600.0,
                  child: Column(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(
                                left: 16.0, top: 24.0, bottom: 8.0),
                            child: pageTitle('Новости'))),
                    StreamBuilder(
                        stream: _news,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          var allNews = snapshot?.data?.values ?? [];

                          var allNewsVisual = [];
                          if (allNews != []) {
                            for (var news in allNews) {
                              allNewsVisual.add(Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 24.0),
                                  child: NewsCard(
                                      news..packet = true, this.widget)));
                            }
                          }
                          return Expanded(
                              child: ListView(
                                  physics: BouncingScrollPhysics(),
                                  padding: const EdgeInsets.all(0.0),
                                  shrinkWrap: true,
                                  children: [...allNewsVisual]));
                        }),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FlatButton(
                            onPressed: () {
                              var newNews = db.root.globalinfo.news.push;
                              newNews.packet = true;
                              Navigator.push(
                                  context,
                                  EnterExitRoute(
                                      exitPage: this.widget,
                                      enterPage: NewsEditPage(newNews)));
                            },
                            child: Text('Добавить новость',
                                style: TextStyle(color: Colors.black45))))
                  ])),
            ),
          ],
        ));
  }
}

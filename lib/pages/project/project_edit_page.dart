import 'package:bubble/bubble.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart' as md;

class ProjectEditPage extends StatefulWidget {
  final project;
  final isUser;

  const ProjectEditPage(this.project, this.isUser, {Key key}) : super(key: key);

  @override
  _ProjectEditPageState createState() => _ProjectEditPageState();
}

class _ProjectEditPageState extends State<ProjectEditPage> {
  var _projectNameController;
  var _photoUrlController;
  var _internalIdController;
  var _mDescriptionController;
  var _statusController;
  var _messageController;

  var _chatStream;
  var _chat;

  @override
  void initState() {
    _chat = db.root.chats[widget.project.chat.value].messages;
    _chatStream = _chat.stream;

    var projectName = widget?.project?.name?.value;
    _projectNameController =
        TextEditingController(text: projectName == 'null' ? '' : projectName);

    var photoUrl = widget?.project?.photoUrl?.value;
    _photoUrlController =
        TextEditingController(text: photoUrl == 'null' ? '' : photoUrl);

    var internalId = widget?.project?.internalId?.value;
    _internalIdController =
        TextEditingController(text: internalId == 'null' ? '' : internalId);

    var mdDescription = widget?.project?.mdDescription?.value;
    _mDescriptionController = TextEditingController(
        text: mdDescription == 'null' ? '' : mdDescription);

    var status = widget?.project?.status?.value;
    _statusController =
        TextEditingController(text: status == 'null' ? '' : status);

    _messageController = TextEditingController();
    super.initState();
  }

  String messageAlignment(message) {
    return widget?.isUser == true && message?.isUser?.value == true
        ? 'Right'
        : widget?.isUser == true && message?.isUser?.value == false
            ? 'Left'
            : widget?.isUser == false && message?.isUser?.value == true
                ? 'Left'
                : widget?.isUser == false && message?.isUser?.value == false
                    ? 'Right'
                    : 'Center';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Align(
            alignment: Alignment.topCenter,
            child: SizedBox(
                // width: MediaQuery.of(context).size.width * 0.7,
                // height: MediaQuery.of(context).size.height,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
              Padding(
                  padding: const EdgeInsets.all(12),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Text('Проект',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold)),
                    TextField(
                        controller: _projectNameController,
                        decoration: InputDecoration(hintText: 'Название'),
                        onChanged: (text) {
                          widget.project.name = text;
                          setState(() {});
                        }),
                    SizedBox(height: 8),
                    TextField(
                        controller: _photoUrlController,
                        decoration:
                            InputDecoration(hintText: 'Изображение(url)'),
                        onChanged: (text) {
                          widget.project.photoUrl = text;
                          setState(() {});
                        }),
                    SizedBox(height: 8),
                    TextField(
                        controller: _internalIdController,
                        decoration: InputDecoration(hintText: 'ID проекта'),
                        onChanged: (text) {
                          widget.project.internalId = text;
                          setState(() {});
                        }),
                    SizedBox(height: 8),
                    TextField(
                        controller: _statusController,
                        decoration: InputDecoration(hintText: 'Статус проекта'),
                        onChanged: (text) {
                          widget.project.status = text;
                          setState(() {});
                        }),
                    SizedBox(height: 8),
                    Row(mainAxisSize: MainAxisSize.min, children: [
                      Flexible(
                          flex: 1,
                          child: TextField(
                              controller: _mDescriptionController,
                              decoration:
                                  InputDecoration(hintText: 'Описание проекта'),
                              onChanged: (text) {
                                setState(
                                    () => widget.project.mdDescription = text);
                              })),
                      Flexible(
                          flex: 1,
                          child: md.Markdown(
                              shrinkWrap: true,
                              data: _mDescriptionController.text))
                    ]),
                    SizedBox(height: 12),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: FlatButton(
                          onPressed:
                              // _projectNameController?.text != '' &&
                              //         _photoUrlController?.text != '' &&
                              //         _internalIdController?.text != '' &&
                              //         _mDescriptionController?.text != ''
                              //     ?
                              () {
                            Navigator.pop(context);
                          },
                          // : null,
                          child: Text('Назад',
                              style: TextStyle(color: Colors.black45))),
                    ),
                  ])),
              StreamBuilder(
                  stream: _chatStream,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    var messages = _chat?.values ?? [];

                    // print('Message: $messages');
                    var messagesUI = [];
                    if (messages != [])
                      for (var message in messages) {
                        if (message?.isUser?.value != null &&
                            message?.title?.value != null) {
                          var side = messageAlignment(message);
                          messagesUI.add(Align(
                              alignment: side == 'Left'
                                  ? Alignment.centerLeft
                                  : side == 'Right'
                                      ? Alignment.centerRight
                                      : Alignment.center,
                              child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Bubble(
                                      nip: side == 'Right'
                                          ? BubbleNip.rightTop
                                          : side == 'Left'
                                              ? BubbleNip.leftTop
                                              : BubbleNip.no,
                                      color: side == 'Right'
                                          ? Color.fromRGBO(225, 255, 199, 1.0)
                                          : Colors.white,
                                      child: Text(message.title.value)))));
                        }
                      }

                    return Expanded(
                        child: Align(
                      alignment: Alignment.centerLeft,
                      child: SizedBox(
                        width: 800,
                        child: ListView(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            children: [...messagesUI]),
                      ),
                    ));
                  }),
              Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: 800,
                  child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Row(children: [
                        Expanded(
                            flex: 3,
                            child: TextField(
                                controller: _messageController,
                                decoration:
                                    InputDecoration(hintText: 'Сообщение'),
                                onChanged: (text) {
                                  setState(() {});
                                })),
                        Expanded(
                            flex: 1,
                            child: IconButton(
                                onPressed: _messageController.text != ''
                                    ? () async {
                                  var allChats = db.root.chats;
                                  var projectChatKey =
                                      widget.project.chat;

                                  if (projectChatKey?.value == 'null') {
                                    var createdChat = allChats.push;
                                    await createdChat.theme
                                        .set('Theme');

                                    projectChatKey.set(createdChat.key);
                                  }

                                  var message =
                                      allChats[projectChatKey.value]
                                          .messages
                                          .push;
                                  message.title =
                                      _messageController.text;
                                  message.isUser = widget.isUser;

                                  // print(DateTime.fromMillisecondsSinceEpoch(DateTime.now().millisecondsSinceEpoch));
                                  // DateTime.now().millisecondsSinceEpoch)

                                  message.timestamp = DateTime.now()
                                      .millisecondsSinceEpoch;
                                  _messageController.clear();
                                        // var allChats = db.root.chats;
                                        // var projectChatKey =
                                        //     widget.project.chat;
                                        //
                                        // if (projectChatKey?.value == 'null') {
                                        //   var createdChat = allChats.push;
                                        //   await createdChat.theme.set('Theme');
                                        //
                                        //   projectChatKey.set(createdChat.key);
                                        // }
                                        //
                                        // var message =
                                        //     allChats[projectChatKey.value]
                                        //         .messages
                                        //         .push;
                                        // message.title = _messageController.text;
                                        // message.isUser = widget.isUser;

                                        // _messageController.clear();
                                      }
                                    : null,
                                icon: Icon(Icons.send)))
                      ])),
                ),
              )
            ]))));
  }
}
// return widget?.isUser == true &&
// message?.isUser?.value == true
// ? Alignment.centerLeft
//     : widget?.isUser == true &&
// message?.isUser?.value ==
// false
// ? Alignment.centerRight
//     : widget?.isUser == false &&
// message?.isUser
//     ?.value ==
// true
// ? Alignment.centerRight
//     : widget?.isUser == false &&
// message?.isUser
//     ?.value ==
// false
// ? Alignment.centerLeft
//     : Alignment.center;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'project_edit_page.dart';
import '../../transitions/enter_exit_route.dart';

class ProjectCard extends StatefulWidget {
  final routeFrom;
  final project;
  final isUser;

  const ProjectCard(this.project, this.routeFrom, this.isUser, {Key key})
      : super(key: key);

  @override
  _ProjectCardState createState() => _ProjectCardState();
}

class _ProjectCardState extends State<ProjectCard> {
  @override
  Widget build(BuildContext context) {
    // print(widget.project.photoUrl.value);
    return Card(
        child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  EnterExitRoute(
                      exitPage: widget.routeFrom,
                      enterPage:
                          ProjectEditPage(widget.project, widget.isUser)));
            },
            child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.project.name.value,
                          style: TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 18)),
                      SizedBox(height: 12.0),
                      CachedNetworkImage(
                          imageUrl: widget.project.photoUrl.value,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Center(child: Icon(Icons.error)))
                    ]))));
  }
}

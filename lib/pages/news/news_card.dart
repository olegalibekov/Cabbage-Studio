import 'package:cabbage_studio/pages/news/news_edit_page.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class NewsCard extends StatefulWidget {
  final news;
  final routeFrom;

  const NewsCard(this.news, this.routeFrom, {Key key}) : super(key: key);

  @override
  _NewsCardState createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  EnterExitRoute(
                      exitPage: widget.routeFrom,
                      enterPage: NewsEditPage(widget.news)));
            },
            child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(children: [
                  SizedBox(height: 8),
                  Row(children: [
                    Text('Название: ', style: TextStyle(fontSize: 18)),
                    Text(widget.news.title.value,
                        style: TextStyle(fontSize: 16))
                  ]),
                  Divider(),
                  SizedBox(height: 8),
                  Row(children: [
                    Text('Описание: ', style: TextStyle(fontSize: 18)),
                    Text(widget.news.shortDescription.value,
                        style: TextStyle(fontSize: 16))
                  ]),
                  SizedBox(height: 8),
                  Divider(),
                  SizedBox(height: 8),
                  Row(children: [
                    Flexible(
                        flex: 1,
                        child: Text('Markdown описание: ',
                            style: TextStyle(fontSize: 18))),
                    Flexible(
                        flex: 1,
                        child: Markdown(
                            shrinkWrap: true,
                            data: widget.news.mdDescription.value))
                  ]),
                  SizedBox(height: 8),
                  Divider(),
                  SizedBox(height: 8),
                  Row(children: [
                    Text('Теги: ', style: TextStyle(fontSize: 18)),
                    Text(widget.news.tags.value, style: TextStyle(fontSize: 16))
                  ]),
                  SizedBox(height: 8),
                  Divider(),
                  SizedBox(height: 8),
                  Row(children: [
                    Text('Ресурсы: ', style: TextStyle(fontSize: 18)),
                    Text(widget.news.resources.value,
                        style: TextStyle(fontSize: 16))
                  ]),
                  SizedBox(height: 8),
                  CachedNetworkImage(
                      imageUrl: widget.news.links.value,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          Center(child: Icon(Icons.error))),
                  SizedBox(height: 8)
                ]))));
  }
}

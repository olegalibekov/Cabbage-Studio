import 'package:flutter/material.dart';

class NewsEditPage extends StatefulWidget {
  final news;

  const NewsEditPage(this.news, {Key key}) : super(key: key);

  @override
  _NewsEditPageState createState() => _NewsEditPageState();
}

class _NewsEditPageState extends State<NewsEditPage> {
  var _titleController;
  var _shortDescriptionController;
  var _mdDescriptionController;
  var _tagsController;
  var _linksController;
  var _resourcesController;

  @override
  void initState() {
    var title = widget?.news?.title?.value;
    _titleController =
        TextEditingController(text: title == 'null' ? '' : title);

    var shortDescription = widget?.news?.shortDescription?.value;
    _shortDescriptionController = TextEditingController(
        text: shortDescription == 'null' ? '' : shortDescription);

    var mdDescription = widget?.news?.mdDescription?.value;
    _mdDescriptionController = TextEditingController(
        text: mdDescription == 'null' ? '' : mdDescription);

    var tags = widget?.news?.tags?.value;
    _tagsController = TextEditingController(text: tags == 'null' ? '' : tags);

    var links = widget?.news?.links?.value;
    _linksController =
        TextEditingController(text: links == 'null' ? '' : links);

    var resources = widget?.news?.resources?.value;
    _resourcesController =
        TextEditingController(text: resources == 'null' ? '' : resources);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Align(
            alignment: Alignment.topCenter,
            child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                height: MediaQuery.of(context).size.height,
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextField(
                          controller: _titleController,
                          decoration: InputDecoration(hintText: 'Название'),
                          onChanged: (text) {
                            widget.news.title = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      TextField(
                          controller: _shortDescriptionController,
                          decoration:
                              InputDecoration(hintText: 'Описание'),
                          onChanged: (text) {
                            widget.news.shortDescription = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      TextField(
                          controller: _mdDescriptionController,
                          decoration:
                              InputDecoration(hintText: 'Markdown описание'),
                          onChanged: (text) {
                            widget.news.mdDescription = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      TextField(
                          controller: _tagsController,
                          decoration: InputDecoration(hintText: 'Теги'),
                          onChanged: (text) {
                            widget.news.tags = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      TextField(
                          controller: _linksController,
                          decoration: InputDecoration(hintText: 'Ссылки'),
                          onChanged: (text) {
                            widget.news.links = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      TextField(
                          controller: _resourcesController,
                          decoration: InputDecoration(hintText: 'Ресурсы'),
                          onChanged: (text) {
                            widget.news.resources = text;
                            setState(() {});
                          }),
                      SizedBox(height: 8),
                      FlatButton(
                          onPressed: _titleController.text != '' ||
                                  _shortDescriptionController.text != '' ||
                                  _mdDescriptionController.text != '' ||
                                  _tagsController.text != '' ||
                                  _linksController.text != '' ||
                                  _resourcesController.text != ''
                              ? () {
                                  Navigator.pop(context);
                                  widget.news.upload();
                                }
                              : null,
                          child: Text('Загрузить',
                              style: TextStyle(color: Colors.black45))),
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Назад',
                              style: TextStyle(color: Colors.black45)))
                    ]))));
  }
}

import 'dart:math';

import 'package:cabbage_studio/main.dart';
import 'package:flutter/material.dart' hide Material;
import 'package:box3d/box3d.dart';
import 'package:zflutter/zflutter.dart';

class MainPageEngine extends StatefulWidget {
  @override
  _MainPageEngineState createState() => _MainPageEngineState();
}

class _MainPageEngineState extends State<MainPageEngine> {
  World world = World();
  Body objectWorld;
  Body object2World;
  double meter = 1.0;
  var size = 2;
  ConvexPolyhedron tetraShape;
  ZShape _zShape;
  final multiplication = 40.0;
  var dt = 1 / 60, damping = 0.5;
  var radius = 1, mass = 2, f = 500;

  createTetra() {
    var tetraPref = 2.0;
    var verts = [
      Vec3(0, 0, 0),
      Vec3(tetraPref, 0, 0),
      Vec3(0, tetraPref, 0),
      Vec3(0, 0, tetraPref)
    ];
    var offset = -0.35;
    for (var i = 0; i < verts.length; i++) {
      var v = verts[i];
      v.x += offset;
      v.y += offset;
      v.z += offset;
    }

    return ConvexPolyhedron(vertices: verts, faces: [
      [0, 3, 2], // -x
      [0, 1, 3], // -y
      [0, 2, 1], // -z
      [1, 2, 3] // +xyz
    ]);
  }

  @override
  void initState() {
    // world.gravity.set(0, 0, 0);
    world.broadphase = NaiveBroadphase();

    tetraShape = createTetra();

    objectWorld = Body(BodyOptions(mass: mass)..position = Vec3(0, 0, 1));
    objectWorld.addShape(tetraShape);
    // objectWorld.linearDamping = objectWorld.angularDamping = damping;
    world.addBody(objectWorld);

    var worldPoint = Vec3(0, 0, 0);
    var impulse = Vec3(f * dt, 0, 0);
    objectWorld.angularVelocity.set(0.2, 0.0, 0.2);
    // objectWorld.applyImpulse(impulse, worldPoint);

    // var objectWorldShape = createTetra();
    // objectWorld = Body(BodyOptions(mass: mass)
    //   ..collisionFilterGroup = 2
    //   ..collisionFilterMask = 1);
    // objectWorld.addShape(objectWorldShape);
    // // objectWorld.position.set(0.0, 0.0, 0.0);
    // // objectWorld.angularVelocity = Vec3(0.15, 0, 0);
    // world.addBody(objectWorld);
    // objectWorld.applyImpulse(Vec3(2.0, 0.0, 0.0), Vec3(0, 0, 0));

    // var object2WorldShape = createTetra();
    // object2World = Body(BodyOptions(mass: mass)
    //   ..collisionFilterGroup = 2
    //   ..collisionFilterMask = 1);
    // object2World.addShape(object2WorldShape);
    // object2World.position.set(0.0, 0.0, 5.0);
    // world.addBody(object2World);

    // var groundShape = Plane();
    // var bottomGroundBody = Body(BodyOptions(mass: 0)
    //   ..collisionFilterGroup = 1
    //   ..collisionFilterMask = 2);
    // bottomGroundBody.addShape(groundShape);
    // bottomGroundBody.position.set(0.0, 0.0, 0.0);
    // world.addBody(bottomGroundBody);

    var fixedTimeStep = 1.0 / 60.0;
    worldUpdate() async {
      world.step(fixedTimeStep, 10, 3);
      setState(() {});
      await Future.delayed(Duration(milliseconds: 1));
      worldUpdate();

      // print(objectWorld.quaternion);
    }

    worldUpdate();

    _zShape = ZShape(path: [
      ZMove.vector(ZVector(
          -tetraShape.vertices[3].x * multiplication,
          -tetraShape.vertices[3].z * multiplication,
          -tetraShape.vertices[3].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[0].x * multiplication,
          -tetraShape.vertices[0].z * multiplication,
          -tetraShape.vertices[0].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[1].x * multiplication,
          -tetraShape.vertices[1].z * multiplication,
          -tetraShape.vertices[1].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[3].x * multiplication,
          -tetraShape.vertices[3].z * multiplication,
          -tetraShape.vertices[3].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[1].x * multiplication,
          -tetraShape.vertices[1].z * multiplication,
          -tetraShape.vertices[1].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[2].x * multiplication,
          -tetraShape.vertices[2].z * multiplication,
          -tetraShape.vertices[2].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[3].x * multiplication,
          -tetraShape.vertices[3].z * multiplication,
          -tetraShape.vertices[3].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[2].x * multiplication,
          -tetraShape.vertices[2].z * multiplication,
          -tetraShape.vertices[2].y * multiplication)),
      ZLine.vector(ZVector(
          -tetraShape.vertices[0].x * multiplication,
          -tetraShape.vertices[0].z * multiplication,
          -tetraShape.vertices[0].y * multiplication))
    ], color: Colors.teal, closed: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: ZDragDetector(builder: (context, controller) {
          return ZIllustration(children: [
            ZPositioned(
                rotate: controller.rotate,
                child: ZGroup(children: [
                  ZPositioned(
                      translate: ZVector(
                          -objectWorld.position.x * multiplication,
                          -objectWorld.position.z * multiplication,
                          -objectWorld.position.y * multiplication),
                      rotate: ZVector(
                          -objectWorld.quaternion.x * multiplication,
                          -objectWorld.quaternion.z * multiplication,
                          -objectWorld.quaternion.y * multiplication),
                      child: _zShape),
                  // ZPositioned(
                  //     translate: ZVector(
                  //         -object2World.position.x * multiplication,
                  //         -object2World.position.z * multiplication,
                  //         -object2World.position.y * multiplication),
                  //     rotate: ZVector(
                  //         -object2World.quaternion.x * multiplication,
                  //         -object2World.quaternion.z * multiplication,
                  //         -object2World.quaternion.y * multiplication),
                  //     child: _zShape)
                ]))
          ]);
        }));
  }
}

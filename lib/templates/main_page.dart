import 'dart:math';

import 'package:cabbage_studio/shapes/pyramid.dart';
import 'package:cabbage_studio/shapes/shape_data.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';
import 'package:flutter/physics.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  final Random _random = Random();

  List<ShapeData> _shapeList = [];

  @override
  void initState() {
    double _pyramidHeight = 200;
    Pyramid _pyramid = Pyramid(150, 50, 10, 1, true);
    var _visualPyramid = _pyramid.withEdgesOpacity(1);

    AnimationController _translationController =
        AnimationController.unbounded(vsync: this);
    const spring = SpringDescription(mass: 1, stiffness: 1, damping: 0);
    Simulation _translationSimulation = SpringSimulation(spring, 0, 1, 0);
    Tween _tween = Tween(begin: Offset(0, 0), end: Offset(0, 0));
    Animation _translationAnimation = _translationController.drive(_tween);
    AnimationController _accelerationController =
        AnimationController(vsync: this);
    AnimationController _rotationController = AnimationController(vsync: this);

    /// Was Tween(begin: _currentVelocity, end: acceleration)
    Tween _accelerationTween = Tween(begin: 0.0, end: 0.0);
    Animation _accelerationAnimation =
        _accelerationTween.animate(_accelerationController);
    double _currentRotation = 0.0;

    ShapeData shapeData = ShapeData(
        _translationController,
        _translationAnimation,
        _translationSimulation,
        _rotationController,
        _accelerationController,
        _accelerationAnimation,
        _tween,
        _accelerationTween,
        _currentRotation,
        _visualPyramid,
        _pyramidHeight,
        _pyramid.pyramidVolume());

    _shapeList.add(shapeData);

    Pyramid _pyramid2 = Pyramid(200, 200, 6, 1, true);

    ShapeData shapeData2 = ShapeData(
        _translationController,
        _translationAnimation,
        _translationSimulation,
        _rotationController,
        _accelerationController,
        _accelerationAnimation,
        _tween,
        _accelerationTween,
        _currentRotation,
        _visualPyramid,
        _pyramidHeight,
        _pyramid2.pyramidVolume());

    _shapeList.add(shapeData2);

    Pyramid _pyramid3 = Pyramid(100, 100, 6, 1, true);
    var _visualPyramid3 = _pyramid3.withEdgesOpacity(1.0);

    ShapeData shapeData3 = ShapeData(
        _translationController,
        _translationAnimation,
        _translationSimulation,
        _rotationController,
        _accelerationController,
        _accelerationAnimation,
        _tween,
        _accelerationTween,
        _currentRotation,
        _visualPyramid3,
        _pyramidHeight,
        _pyramid3.pyramidVolume());

    _shapeList.add(shapeData3);

    extremeVolume(bool max) {
      return _shapeList
          .reduce((curr, next) {
            if (max) {
              return curr.shapeVolume > next.shapeVolume ? curr : next;
            } else {
              return curr.shapeVolume < next.shapeVolume ? curr : next;
            }
          })
          .shapeVolume
          .toDouble();
    }

    double _shapeListMinVolume = extremeVolume(false);
    double _shapeListMaxVolume = extremeVolume(true);

    if (_shapeList.length > 1) {
      for (int shapeIndex = 0; shapeIndex < _shapeList.length; shapeIndex++) {
        var shape = _shapeList[shapeIndex];

        double shapeDecelerationQuotient =
            (shape.shapeVolume - _shapeListMinVolume) /
                (_shapeListMaxVolume - _shapeListMinVolume);
        double shapeMaxVelocity = minVelocity +
            (1 - shapeDecelerationQuotient) * (maxVelocity - minVelocity);

        shape.shapeMaxVelocity = shapeMaxVelocity;
        shape.shapeDecelerationQuotient = shapeDecelerationQuotient;
      }
    }

    // _translationController.addListener(() {
    //   if (_translationController.velocity > 0.00 &&
    //       _translationController.velocity < 0.002) {
    //     double _randomX = _random.nextInt(50) - 25.0;
    //     double _randomY = _random.nextInt(50) - 25.0;
    //     _runTranslation(_offsetAnimation, Offset(_randomX, _randomY));
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: ZDragDetector(builder: (context, controller) {
          return ZIllustration(children: [
            ZPositioned(
                rotate: controller.rotate,
                child: ZGroup(children: [
                  for (int shapeIndex = 0;
                      // shapeIndex < _shapeList.length;
                      shapeIndex < 1;
                      shapeIndex++)
                    AnimatedBuilder(
                        animation: Listenable.merge([
                          _shapeList[shapeIndex].translationController,
                          _shapeList[shapeIndex].rotationController
                        ]),
                        child: _shapeList[shapeIndex].shape,
                        builder: (BuildContext context, Widget child) {
                          // Offset offsetAnimation;
                          if (_shapeList[shapeIndex]
                                  .translationAnimation
                                  ?.value !=
                              null) {
                            _shapeList[shapeIndex].offsetAnimation =
                                _shapeList[shapeIndex]
                                    .translationAnimation
                                    .value;
                          }
                          return ZPositioned(
                              translate: ZVector(
                                  _shapeList[shapeIndex].offsetAnimation.dx,
                                  _shapeList[shapeIndex].offsetAnimation.dy,
                                  0),
                              rotate: ZVector(pi / 4,
                                  _shapeList[shapeIndex].currentRotation, 0),
                              child: ZGroup(children: [
                                ZPositioned(
                                    translate: ZVector(
                                        0,
                                        _shapeList[shapeIndex].shapeHeight / 3,
                                        0),
                                    child: child)
                              ]));
                        }),
                  ZPositioned(
                      translate: ZVector(350, -100, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                double _randomX = _random.nextInt(30) - 15.0;
                                double _randomY = _random.nextInt(30) - 15.0;
                                _shapeList[0].runTranslation(
                                    _shapeList[0].offsetAnimation,
                                    Offset(_randomX, _randomY));
                              },
                              child: Text('Change direction')))),
                  ZPositioned(
                      translate: ZVector(350, 0, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                _shapeList[0].runTranslation(
                                    _shapeList[0].offsetAnimation,
                                    Offset(-25.0, 50.0));
                              },
                              child: Text('Animate')))),
                  ZPositioned(
                      translate: ZVector(350, 100, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                _shapeList[0].accelerationRotation(
                                    _shapeList[0].neutralVelocity);
                              },
                              child: Text('Passive forward')))),
                  ZPositioned(
                      translate: ZVector(350, 200, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                _shapeList[0].accelerationRotation(
                                    -_shapeList[0].neutralVelocity);
                              },
                              child: Text('Passive back')))),
                  ZPositioned(
                      translate: ZVector(350, 300, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                _shapeList[0].accelerationRotation(
                                    _shapeList[0].shapeMaxVelocity);
                              },
                              child: Text('Acceleration forward')))),
                  ZPositioned(
                      translate: ZVector(150, 300, 0),
                      child: ZToBoxAdapter(
                          width: 200,
                          height: 100,
                          child: FlatButton(
                              onPressed: () {
                                _shapeList[0].accelerationRotation(
                                    -_shapeList[0].shapeMaxVelocity);
                              },
                              child: Text('Acceleration back'))))
                ]))
          ]);
        }));
  }
}

import 'package:flutter/material.dart';

class PageSwitchingTwo extends StatefulWidget {
  @override
  _PageSwitchingTwoState createState() => _PageSwitchingTwoState();
}

class _PageSwitchingTwoState extends State<PageSwitchingTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.orange,
        body: Center(
            child: FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Push'))));
  }
}

// import 'dart:math';
//
// import 'package:flutter/material.dart';
// import 'package:zflutter/zflutter.dart';
//
// class Shapes extends StatefulWidget {
//   @override
//   _ShapesState createState() => _ShapesState();
// }
//
// class _ShapesState extends State<Shapes> {
//   parallelepiped(double width, double height) {
//     ZToBoxAdapter boxContainer(
//         double boxWidth, double boxHeight, Color boxColor) {
//       return ZToBoxAdapter(
//           width: boxWidth,
//           height: boxHeight,
//           child: Container(color: boxColor));
//     }
//
//     return ZGroup(children: [
//       ZBox(
//         height: height,
//         width: width,
//         depth: height,
//         color: Colors.red,
//         // frontColor: Color(0xffCC2255),
//         // topColor: Colors.yellow,
//         // leftColor: Colors.green,
//         // rightColor: Colors.blue,
//         // bottomColor: Colors.orange,
//         // rearColor: Colors.red,
//       ),
//       ZPositioned(
//           translate: ZVector(-width / 2, 0, 0),
//           rotate: ZVector(0, pi / 2, 0),
//           child: boxContainer(height, height, Colors.cyan)),
//       ZPositioned(
//           translate: ZVector(width / 2, 0, 0),
//           rotate: ZVector(0, pi / 2, 0),
//           child: boxContainer(height, height, Colors.teal)),
//       ZPositioned(
//           translate: ZVector(0, -height / 2, 0),
//           rotate: ZVector(pi / 2, 0, 0),
//           child: boxContainer(width, height, Colors.cyanAccent)),
//       ZPositioned(
//           translate: ZVector(0, height / 2, 0),
//           rotate: ZVector(pi / 2, 0, 0),
//           child: boxContainer(width, height, Colors.tealAccent)),
//       ZPositioned(
//           translate: ZVector(0, 0, -height / 2),
//           child: boxContainer(width, height, Colors.blue)),
//       ZPositioned(
//           translate: ZVector(0, 0, height / 2),
//           child: boxContainer(width, height, Colors.teal))
//     ]);
//   }
//
//   List<ZVector> pyramidBase(int radius, int faces) {
//     bool pathBeginning = true;
//     List<ZVector> pyramidBasePath = [];
//
//     List<int> xPoints = [];
//     List<int> yPoints = [];
//
//     int x = 0;
//     int y = 0;
//     int R = radius;
//     int k = faces;
//     int angle = 0;
//     int i = 0;
//     k = 360 ~/ k;
//
//     while (angle <= 360 + k) {
//       i = i + 1;
//       xPoints.add((x + cos(angle / 180 * pi) * R).round());
//       yPoints.add((y - sin(angle / 180 * pi) * R).round());
//       angle = angle + k;
//     }
//
//     while (i - 2 > 0) {
//       i = i - 1;
//       if (pathBeginning) {
//         pyramidBasePath
//             .add(ZVector(xPoints[i].toDouble(), 0.0, yPoints[i].toDouble()));
//         pyramidBasePath.add(
//             ZVector(xPoints[i - 1].toDouble(), 0.0, yPoints[i - 1].toDouble()));
//         pathBeginning = false;
//       } else {
//         pyramidBasePath.add(
//             ZVector(xPoints[i - 1].toDouble(), 0.0, yPoints[i - 1].toDouble()));
//       }
//     }
//
//     return pyramidBasePath;
//   }
//
//   double distance(ZVector pointA, ZVector pointB) {
//     return sqrt(pow((pointB.x - pointA.x), 2) +
//         pow((pointB.y - pointA.y), 2) +
//         pow((pointB.z - pointA.z), 2));
//   }
//
//   ZWidget createPyramidWithContainers(
//       int baseRadius, int faces, double height) {
//     List<ZVector> pyramidBasePoints = pyramidBase(baseRadius, faces);
//     ZVector topPointPosition = ZVector(0.0, -height, 0.0);
//
//     ZWidget createPyramidShape() {
//       List<ZPathCommand> pyramidBottomZPath = [];
//
//       pyramidBottomZPath.add(ZMove.vector(pyramidBasePoints[0]));
//       for (int bottomPoint = 1;
//           bottomPoint < pyramidBasePoints.length;
//           bottomPoint++)
//         pyramidBottomZPath.add(ZLine.vector(pyramidBasePoints[bottomPoint]));
//
//       List<ZPathCommand> pyramidTopZPath = [];
//
//       for (int bottomPoint = 1;
//           bottomPoint < pyramidBasePoints.length;
//           bottomPoint++) {
//         pyramidTopZPath.add(ZMove.vector(topPointPosition));
//         pyramidTopZPath.add(ZLine.vector(pyramidBasePoints[bottomPoint]));
//       }
//
//       return ZShape(
//           path: [...pyramidBottomZPath, ...pyramidTopZPath], color: Colors.red);
//     }
//
//     ZWidget pyramidShape = createPyramidShape();
//
//     double clipPathTriangleWidth =
//         distance(pyramidBasePoints[0], pyramidBasePoints[1]);
//     double clipPathTriangleHeight =
//         distance(ZVector(baseRadius.toDouble(), 0.0, 0.0), topPointPosition);
//     Size clipPathTriangleSize =
//         Size(clipPathTriangleWidth, clipPathTriangleHeight);
//
//     double pointsDistanceFirst =
//         distance(pyramidBasePoints[0], pyramidBasePoints[1]) / 2;
//
//     double pointsDistanceSecond =
//         distance(ZVector(0, 0, 0), pyramidBasePoints[1]);
//
//     double cathetus =
//         sqrt(pow(pointsDistanceSecond, 2) - pow(pointsDistanceFirst, 2));
//
//     double clipPathTriangleAngleInRadians =
//         atan(cathetus / -topPointPosition.y);
//
//     List<ZPositioned> containersPosition = [];
//
//     double degreesToRads(double deg) => (deg * pi) / 180.0;
//
//     final angleBetweenFaces = degreesToRads(360 / faces);
//     for (int currentFace = 0; currentFace < faces; currentFace++) {
//       var visualFace = ZPositioned(
//           translate: ZVector(
//               (pyramidBasePoints[currentFace].x +
//                       pyramidBasePoints[currentFace + 1].x) /
//                   2,
//               0,
//               (pyramidBasePoints[currentFace].z +
//                       pyramidBasePoints[currentFace + 1].z) /
//                   2),
//           rotate: ZVector(
//               0, -pi / faces + pi / 2 + currentFace * angleBetweenFaces, 0.0),
//           child: ZGroup(children: [
//             ZPositioned(
//                 rotate: ZVector(-clipPathTriangleAngleInRadians, 0, 0),
//                 child: ZGroup(children: [
//                   ZPositioned(
//                       translate:
//                           ZVector(0, -clipPathTriangleSize.height / 2, 0),
//                       child: ZToBoxAdapter(
//                           width: clipPathTriangleSize.width,
//                           height: clipPathTriangleSize.height,
//                           child: ClipPath(
//                               child: Container(color: Colors.purple),
//                               clipper: PyramidFaceClipper())))
//                 ]))
//           ]));
//       containersPosition.add(visualFace);
//     }
//
//     ZWidget pyramidWithContainers = ZPositioned(
//         rotate: ZVector(0, pi / faces, 0),
//         child: ZGroup(children: [pyramidShape, ...containersPosition]));
//     return pyramidWithContainers;
//   }
//
//   get pyramid => createPyramidWithContainers(250, 18, 400);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
// body: ZDragDetector(builder: (context, controller) {
//       return ZIllustration(
//           children: [ZPositioned(rotate: controller.rotate, child: pyramid)]);
//     }));
//   }
// }
//
// class PyramidFaceClipper extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     Path path = Path()
//       ..moveTo(size.width / 2, 0)
//       ..lineTo(0, size.height)
//       ..lineTo(size.width, size.height)
//       ..close();
//
//     return path;
//   }
//
//   @override
//   bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
// }
//
// // int n; //кол-во граней
// // int R; //радиус
// // int X, Y; //координаты центра
// // List<int> x = []; //координаты точек
// // List<int> y = [];
// // int x1, y1, x2, y2;
// // int j;
//
// // List<ZPathCommand> pyramidBase() {
// //   List<ZPathCommand> pyramidBasePath = [];
// //
// //   bool pathBeginning = true;
// //
// //   double a, b, z = 0;
// //   int i = 0;
// //   double angle = 360.0 / n;
// //   //цикл создающий массив из точек
// //   while (i < n) {
// //     a = cos(z / 180 * pi);
// //     b = sin(z / 180 * pi);
// //     // print(i);
// //     x.add(X + (a.round() * R).toInt());
// //     y.add(Y - (b.round() * R).toInt());
// //     z = z + angle;
// //     i++;
// //   }
// //
// //   int x1, x2, y1, y2;
// //
// //   int j = n - 1; //цикл передающий координаты для прорисовки грани
// //   while (j >= 0) {
// //     if (j > 0) {
// //       x1 = x[j];
// //       x2 = x[j - 1];
// //       y1 = y[j];
// //       y2 = y[j - 1];
// //       if (pathBeginning) {
// //         pyramidBasePath
// //             .add(ZMove.vector(ZVector(x1.toDouble(), 0, y1.toDouble())));
// //         pyramidBasePath
// //             .add(ZLine.vector(ZVector(x2.toDouble(), 0, y2.toDouble())));
// //         pathBeginning = false;
// //       } else {
// //         pyramidBasePath
// //             .add(ZLine.vector(ZVector(x2.toDouble(), 0, y2.toDouble())));
// //
// //       }
// //
// //       // print('$x1, $y1, $x2, $y2');
// //       // g.drawLine(x1, y1, x2, y2);
// //       j--;
// //     } else {
// //       x1 = x[j];
// //       x2 = x[n - 1];
// //       y1 = y[j];
// //       y2 = y[n - 1];
// //       pyramidBasePath
// //           .add(ZLine.vector(ZVector(x2.toDouble(), 0, y2.toDouble())));
// //       pathBeginning = false;
// //       // print('$x1, $y1, $x2, $y2');
// //       // g.drawLine(x1, y1, x2, y2);
// //       j--;
// //     }
// //   }
// //   return pyramidBasePath;
// // }
//
// // n = 8; //кол-во граней
// // R = 100; //радиус
// // X = 0;
// // Y = 0; //координаты центра
// // j = n;
//
// // pyramidBase();
//
// // class Way {
// //   double up;
// //   double right;
// //   double forward;
// //   double radius;
// //
// //   Way({this.up, this.right, this.forward, this.radius});
// // }
//
// // List<Widget> cubeWithVideo = [];
// // cubeWithVideo.add(ZGroup(
// // children: [
// // ZPositioned(
// // translate: ZVector(0, 0, -50),
// // child: ZPositioned(
// // rotate: ZVector(0, 0, 0),
// // child: ZToBoxAdapter(
// // height: 100,
// // width: 100,
// // child: Container(
// // height: 100,
// // width: 100,
// // color: Colors.blue,
// // ),
// // ),
// // ),
// // ),
// // ZPositioned(
// // translate: ZVector(0, 0, 50),
// // child: ZPositioned(
// // rotate: ZVector(0, 0, 0),
// // child: ZToBoxAdapter(
// // height: 100,
// // width: 100,
// // child: Container(
// // height: 100,
// // width: 100,
// // color: Colors.blueGrey,
// // ),
// // ),
// // ),
// // ),
// // ZPositioned(
// // //          rotate: ZVector(pi / 6, pi / 6, 0),
// // //          translate: ZVector(-screenSize.width / 2, -screenSize.height / 3, 0),
// // child: ZGroup(
// // children: parallelepipedFull(Point3D(0, 0, 0),
// // a: 100,
// // b: 100,
// // c: 100,
// // thick: 2,
// // filling: false,
// // color: Colors.amber),
// // ),
// // ),
// // ],
// // ));
//
// // class Point3D extends ZVector {
// //   Point3D(double x, double y, double z) : super(x, y, z);
// //
// //   double qA2(Point3D A, Point3D I) {
// //     double result = (A.x - I.x) * (A.x - I.x) +
// //         (A.y - I.y) * (A.y - I.y) +
// //         (A.z - I.z) * (A.z - I.z);
// //     return result;
// //   }
// //
// //   Point3D strokePoint(Point3D A, Point3D I) {
// //     double qa2 = sqrt(qA2(A, I));
// //     double strokeAX = A.x + ((A.x - I.x) * 300 * (200 / qa2)) / qa2;
// //     double strokeAY = A.y + ((A.y - I.y) * 300 * (200 / qa2)) / qa2;
// //     double strokeAZ = A.z + ((A.z - I.z) * 300 * (200 / qa2)) / qa2;
// //     return Point3D(strokeAX, strokeAY, strokeAZ);
// //   }
// //
// //   static double distTwoPoints(Point3D A, Point3D B) {
// //     double a = (A.x - B.x) * (A.x - B.x);
// //     double b = (A.y - B.y) * (A.y - B.y);
// //     double c = (A.z - B.z) * (A.z - B.z);
// //     return sqrt(a + b + c);
// //   }
// // }
//
// // static List<ZShape> quadrilateralPoints(
// // Point3D A,
// //     Point3D B,
// // Point3D C,
// //     Point3D D, {
// // bool filling = false,
// // double scenario = 0,
// // //double up = 0, double right = 0, double forward = 0, double radius = 0
// // Way w,
// // Point3D med,
// // double axisX = 0,
// // double axisY = 0,
// // double axisZ = 0,
// // bool rotation = false,
// // Color color = Colors.black,
// // double thick = 1,
// // ZVector translate,
// // double axisStartX = 0,
// // double axisStartY = 0,
// // double axisStartZ = 0,
// // }) {
// // w = w ?? Way(up: 0, forward: 0, right: 0, radius: 0);
// // double right = w.right;
// // double forward = w.forward;
// // double up = w.up;
// // double radius = w.radius;
// //
// // //(scenario - 1 + w.coef) / (w.coef) ;
// //
// // double X = right * scenario - (radius - radius * cos(2 * pi * scenario));
// // double Y = -up * pow(sin(pow((scenario) * sqrt(sqrt(5 * pi / 2)), 4)), 2) -
// // radius * sin(2 * pi * scenario);
// // double Z = forward * scenario;
// //
// // med = med ??
// // Point3D((A.x + B.x + C.x + D.x) / 4, (A.y + B.y + C.y + D.y) / 4,
// // (A.z + B.z + C.z + D.z) / 4);
// // translate = translate ?? ZVector.zero;
// //
// // double medX = X + med.x;
// // double medY = Y + med.y;
// // double medZ = Z + med.z;
// //
// // if (rotation) {
// // axisX = scenario * axisX * 2 * pi / 360 + axisStartX * 2 * pi / 360;
// // axisY = scenario * axisY * 2 * pi / 360 + axisStartY * 2 * pi / 360;
// // axisZ = scenario * axisZ * 2 * pi / 360 + axisStartZ * 2 * pi / 360;
// // } else {
// // axisX = axisX * 2 * pi / 360;
// // axisY = axisY * 2 * pi / 360;
// // axisZ = axisZ * 2 * pi / 360;
// // }
// //
// // double AX = (A.x + X) - medX;
// // double BX = (B.x + X) - medX;
// // double CX = (C.x + X) - medX;
// // double DX = (D.x + X) - medX;
// // double AY = (A.y + Y) - medY;
// // double BY = (B.y + Y) - medY;
// // double CY = (C.y + Y) - medY;
// // double DY = (D.y + Y) - medY;
// // double AZ = (A.z + Z) - medZ;
// // double BZ = (B.z + Z) - medZ;
// // double CZ = (C.z + Z) - medZ;
// // double DZ = (D.z + Z) - medZ;
// //
// // double OAX;
// // double OBX;
// // double OCX;
// // double ODX;
// // double OAY;
// // double OBY;
// // double OCY;
// // double ODY;
// // double OAZ;
// // double OBZ;
// // double OCZ;
// // double ODZ;
// //
// // // X axis turn
// // OAX = (AX);
// // OBX = (BX);
// // OCX = (CX);
// // ODX = (DX);
// //
// // OAY = (AY) * cos(axisX) + (AZ) * (sin(axisX));
// // OBY = (BY) * cos(axisX) + (BZ) * (sin(axisX));
// // OCY = (CY) * cos(axisX) + (CZ) * (sin(axisX));
// // ODY = (DY) * cos(axisX) + (DZ) * (sin(axisX));
// //
// // OAZ = (AZ) * cos(axisX) - (AY) * sin(axisX);
// // OBZ = (BZ) * cos(axisX) - (BY) * sin(axisX);
// // OCZ = (CZ) * cos(axisX) - (CY) * sin(axisX);
// // ODZ = (DZ) * cos(axisX) - (DY) * sin(axisX);
// //
// // AX = OAX;
// // BX = OBX;
// // CX = OCX;
// // DX = ODX;
// // AY = OAY;
// // BY = OBY;
// // CY = OCY;
// // DY = ODY;
// // AZ = OAZ;
// // BZ = OBZ;
// // CZ = OCZ;
// // DZ = ODZ;
// //
// // //Y axis turn
// // OAX = (AX) * cos(axisY) + (AZ) * sin(axisY);
// // OBX = (BX) * cos(axisY) + (BZ) * sin(axisY);
// // OCX = (CX) * cos(axisY) + (CZ) * sin(axisY);
// // ODX = (DX) * cos(axisY) + (DZ) * sin(axisY);
// //
// // OAY = AY;
// // OBY = BY;
// // OCY = CY;
// // ODY = DY;
// //
// // OAZ = (AZ) * cos(axisY) - (AX) * sin(axisY);
// // OBZ = (BZ) * cos(axisY) - (BX) * sin(axisY);
// // OCZ = (CZ) * cos(axisY) - (CX) * sin(axisY);
// // ODZ = (DZ) * cos(axisY) - (DX) * sin(axisY);
// //
// // AX = OAX;
// // BX = OBX;
// // CX = OCX;
// // DX = ODX;
// // AY = OAY;
// // BY = OBY;
// // CY = OCY;
// // DY = ODY;
// // AZ = OAZ;
// // BZ = OBZ;
// // CZ = OCZ;
// // DZ = ODZ;
// //
// // //Z axis turn
// // OAX = (AX) * cos(axisZ) - (AY) * sin(axisZ);
// // OBX = (BX) * cos(axisZ) - (BY) * sin(axisZ);
// // OCX = (CX) * cos(axisZ) - (CY) * sin(axisZ);
// // ODX = (DX) * cos(axisZ) - (DY) * sin(axisZ);
// //
// // OAY = (AY) * cos(axisZ) + (AX) * sin(axisZ);
// // OBY = (BY) * cos(axisZ) + (BX) * sin(axisZ);
// // OCY = (CY) * cos(axisZ) + (CX) * sin(axisZ);
// // ODY = (DY) * cos(axisZ) + (DX) * sin(axisZ);
// //
// // OAZ = AZ;
// // OBZ = BZ;
// // OCZ = CZ;
// // ODZ = DZ;
// //
// // AX = OAX;
// // BX = OBX;
// // CX = OCX;
// // DX = ODX;
// // AY = OAY;
// // BY = OBY;
// // CY = OCY;
// // DY = ODY;
// // AZ = OAZ;
// // BZ = OBZ;
// // CZ = OCZ;
// // DZ = ODZ;
// //
// // List<ZShape> planes = [];
// // ZShape plane = ZShape(path: [
// // ZMove.only(
// // x: AX + medX + translate.x,
// // y: AY + medY - translate.y,
// // z: AZ + medZ - translate.z),
// // ZLine.only(
// // x: BX + medX + translate.x,
// // y: BY + medY - translate.y,
// // z: BZ + medZ - translate.z),
// // ZLine.only(
// // x: CX + medX + translate.x,
// // y: CY + medY - translate.y,
// // z: CZ + medZ - translate.z),
// // ZLine.only(
// // x: DX + medX + translate.x,
// // y: DY + medY - translate.y,
// // z: DZ + medZ - translate.z),
// // ], closed: true, stroke: thick, fill: filling, color: color);
// // planes.add(plane);
// // return planes;
// // }
// //
// // static List<ZShape> parallelepipedFull(
// // Point3D O, {
// // double a = 10,
// //     double b = 10,
// // double c = 10,
// //     double scenario = 0,
// // //double up = 0, double right = 0, double forward = 0, double radius = 0
// // Way w,
// //     double axisX = 0,
// // double axisY = 0,
// //     double axisZ = 0,
// // bool filling = true,
// //     Color color = Colors.black,
// // double thick = 1,
// //     ZVector translate,
// // bool firstEdgeFilling = true,
// //     bool secondEdgeFilling = true,
// // bool thirdEdgeFilling = true,
// //     bool fourthEdgeFilling = true,
// // bool fifthEdgeFilling = true,
// //     bool sixthEdgeFilling = true,
// // }) {
// // w = w ?? Way(up: 0, forward: 0, right: 0, radius: 0);
// // double right = w.right;
// // double forward = w.forward;
// // double up = w.up;
// // double radius = w.radius;
// //
// // Point3D med = Point3D(O.x, O.y, O.z);
// //
// // double X = O.x;
// // double Y = O.y;
// // double Z = O.z;
// //
// // List<ZShape> parallelepiped = [];
// //
// // Point3D A1 = Point3D(X - a / 2, Y - b / 2, Z + c / 2);
// // Point3D B1 = Point3D(X - a / 2, Y + b / 2, Z + c / 2);
// // Point3D C1 = Point3D(X + a / 2, Y + b / 2, Z + c / 2);
// // Point3D D1 = Point3D(X + a / 2, Y - b / 2, Z + c / 2);
// //
// // Point3D A2 = Point3D(X - a / 2, Y - b / 2, Z - c / 2);
// // Point3D B2 = Point3D(X - a / 2, Y + b / 2, Z - c / 2);
// // Point3D C2 = Point3D(X + a / 2, Y + b / 2, Z - c / 2);
// // Point3D D2 = Point3D(X + a / 2, Y - b / 2, Z - c / 2);
// //
// // parallelepiped.addAll(quadrilateralPoints(A1, B1, C1, D1,
// // filling: firstEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// // parallelepiped.addAll(quadrilateralPoints(A1, B1, B2, A2,
// // filling: secondEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// // parallelepiped.addAll(quadrilateralPoints(A2, B2, C2, D2,
// // filling: sixthEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// // parallelepiped.addAll(quadrilateralPoints(C2, D2, D1, C1,
// // filling: fifthEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// // parallelepiped.addAll(quadrilateralPoints(C2, B2, B1, C1,
// // filling: thirdEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// // parallelepiped.addAll(quadrilateralPoints(A2, D2, D1, A1,
// // filling: fourthEdgeFilling,
// // color: color,
// // thick: thick,
// // scenario: scenario,
// // w: w,
// // med: med,
// // axisX: axisX,
// // axisY: axisY,
// // axisZ: axisZ,
// // rotation: true,
// // translate: translate));
// //
// // return (parallelepiped);
// // }

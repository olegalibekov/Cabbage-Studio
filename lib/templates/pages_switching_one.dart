import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:flutter/material.dart';

import 'page_switching_two.dart';

class PageSwitchingOne extends StatefulWidget {
  @override
  _PageSwitchingOneState createState() => _PageSwitchingOneState();
}

class _PageSwitchingOneState extends State<PageSwitchingOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.cyan,
        body: Center(
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: PageSwitchingOne(),
                          enterPage: PageSwitchingTwo()));
                },
                child: Text('Push'))));
  }
}

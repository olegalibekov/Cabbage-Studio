import 'dart:math';

import 'package:cabbage_studio/custom_widgets/custom_drag_detector.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';
import 'package:marquee/marquee.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUsPage>
    with SingleTickerProviderStateMixin {
  // var _projectsCubeTop;
  var _projectsCubeCenter;
  var _projectsCubeBottom;

  var _rotateAroundValue = 0.0;
  var _dragStartValue;
  var _snapStartValue;

  var _snapAnimationController;
  var _snapTween;
  var _snapAnimation;

  var _rotatesNumber = 0;
  var _currentFace = 1;

  var _cubeElementsArray = [0, 1, 2, 3, 4, 5, 6, 7];
  var _currentArrayIndex = 0;
  var _frontValue;
  var _rightSideValue;
  var _backValue;
  var _leftSideValue;

  @override
  void initState() {
    // _projectsCubeTop = topBox(250, 350, 250);
    _projectsCubeCenter = middleBox(250, 100, 250);
    _projectsCubeBottom = bottomBox(250, 50, 250);
    _snapAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));

    _snapTween = Tween<double>(begin: 0.0, end: 0.0);
    _snapAnimation = _snapTween.animate(CurvedAnimation(
        curve: Curves.easeInQuad, parent: _snapAnimationController));

    _frontValue = _cubeElementsArray[0];
    _rightSideValue = _cubeElementsArray[1];
    _backValue = _cubeElementsArray[2];
    // _backValue = _cubeElementsArray[2];
    // _leftSideValue = _cubeElementsArray[];

    _snapAnimationController.addListener(() {
      _rotateAroundValue = _snapAnimation.value;

      _currentArrayIndex = normalizedArrayIndex(_rotatesNumber);

      // _frontValue = _cubeElementsArray[_currentArrayIndex];
      // _rightSideValue =
      // _cubeElementsArray[(_rotatesNumber + 1) % _cubeElementsArray.length];
      // _backValue =
      //     _cubeElementsArray[(_rotatesNumber + 2) % _cubeElementsArray.length];
      // _backValue = _cubeElementsArray[_currentArrayIndex + 2];
      // _leftSideValue = _cubeElementsArray[ - 1];
      // queueIndex(_currentArrayIndex + 1);
    });

    super.initState();
  }

  normalizedArrayIndex(int receivedIndex) =>
      receivedIndex % _cubeElementsArray.length;

  closestAngle(double a, double b) {
    var c1 = a - (a % b);
    var c2 = (a + b) - (a % b);
    if (a - c1 > c2 - a) {
      return c2;
    } else {
      return c1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          return GestureDetector(
              onHorizontalDragStart: (event) {
                _dragStartValue = event.localPosition.dx;
                _snapStartValue = _snapTween.end;
              },
              onHorizontalDragUpdate: (event) {
                var fullAngleRotation =
                    (_dragStartValue - event.localPosition.dx) /
                        (constraints.maxWidth / 3);

                _rotateAroundValue = fullAngleRotation * pi / 2;
                var _snapTweenInstantRotate =
                    _snapStartValue + _rotateAroundValue;
                _snapTween.begin = _snapTweenInstantRotate;
                _snapTween.end = _snapTweenInstantRotate;
                _snapAnimationController
                  ..reset()
                  ..duration = Duration.zero
                  ..forward();
              },
              onHorizontalDragEnd: (event) {
                var currentClosestAngle =
                    closestAngle(_rotateAroundValue, pi / 2);

                _snapTween.begin = _snapTween.end;
                _snapTween.end = currentClosestAngle;
                _snapAnimationController
                  ..reset()
                  ..duration = Duration(milliseconds: 300)
                  ..forward();

                _rotatesNumber = (currentClosestAngle ~/ (pi / 2));
                _currentFace = (_rotatesNumber) % 4 + 1;
              },
              child: ZIllustration(children: [
                AnimatedBuilder(
                    animation: _snapAnimationController,
                    builder: (BuildContext context, Widget child) {
                      // if (_currentFace % 5 == 0) {
                      //   _currentFace = 1;
                      // } else {
                      //   _currentFace +=  1;
                      // }

                      // print(_rotatesNumber % _cubeElementsArray.length);

                      return ZPositioned(
                          rotate: ZVector(0.0, _rotateAroundValue, 0.0),
                          child: ZGroup(children: [
                            ZPositioned(
                                translate: ZVector.only(y: -100),
                                child: topBox(250, 350, 250)),
                            ZPositioned(
                                translate: ZVector.only(y: 150),
                                child: _projectsCubeCenter),
                            ZPositioned(
                                translate: ZVector.only(y: 250),
                                rotate: ZVector(0, 0.0, 0.0),
                                child: _projectsCubeBottom)
                          ]));
                    })
              ]));
        }));
  }

  topBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Color boxColor,
        [int content]) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Center(
              child: content != null
                  ? Text(content.toString(), style: TextStyle(fontSize: 30.0))
                  : Container()));
    }

    return ZGroup(children: [
      ZBox(height: height, width: width, depth: depth, color: Colors.black),
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.green)),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.yellow)),
      ZPositioned(
          translate: ZVector(0, -height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.lightGreen)),
      ZPositioned(
          translate: ZVector(0, height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.tealAccent)),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2),
          child: boxContainer(width, height, Colors.red)),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2),
          child: boxContainer(width, height, Colors.orange,
              _cubeElementsArray[normalizedArrayIndex(_currentArrayIndex)]
              // _cubeElementsArray[normalizedArrayIndex(_currentArrayIndex -
              //     _rotatesNumber.sign * (1 - _currentFace).abs())]
              ))
    ]);
  }

  middleBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Color boxColor) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Center(
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0),
                  child: Marquee(
                      text:
                          'An app description is an play store optimized product definition. It greatly influences your product success. There are three components of an app definition: its Name, its Description in the marketplace, and Screenshots. ',
                      style: TextStyle(color: Colors.black, fontSize: 20.0)))));
    }

    return ZGroup(children: [
      // ZBox(height: height, width: width, depth: depth, color: Colors.cyan),
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.green)),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.yellow)),
      ZPositioned(
          translate: ZVector(0, -height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.lightGreen)),
      ZPositioned(
          translate: ZVector(0, height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.tealAccent)),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2),
          child: boxContainer(width, height, Colors.red)),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2),
          child: boxContainer(width, height, Colors.orange))
    ]);
  }

  bottomBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Color boxColor) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Center(child: Text('BoxName')));
    }

    return ZGroup(children: [
      ZBox(height: height, width: width, depth: depth, color: Colors.black),
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.green)),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.yellow)),
      ZPositioned(
          translate: ZVector(0, -height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.lightGreen)),
      ZPositioned(
          translate: ZVector(0, height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.tealAccent)),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2),
          child: boxContainer(width, height, Colors.red)),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2),
          child: boxContainer(width, height, Colors.orange))
    ]);
  }

  parallelepiped(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Color boxColor) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Container(color: boxColor));
    }

    return ZGroup(children: [
      ZBox(
        height: height,
        width: width,
        depth: depth,
        color: Colors.red,
        // frontColor: Color(0xffCC2255),
        // topColor: Colors.yellow,
        // leftColor: Colors.green,
        // rightColor: Colors.blue,
        // bottomColor: Colors.orange,
        // rearColor: Colors.red,
      ),
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.green)),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, Colors.yellow)),
      ZPositioned(
          translate: ZVector(0, -height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.lightGreen)),
      ZPositioned(
          translate: ZVector(0, height / 2, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, Colors.tealAccent)),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2),
          child: boxContainer(width, height, Colors.red)),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2),
          child: boxContainer(width, height, Colors.orange))
    ]);
  }
}

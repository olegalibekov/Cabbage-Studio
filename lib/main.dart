import 'package:cabbage_studio/frames/hero_project_page.dart';
import 'package:cabbage_studio/frames/menu_page.dart';
import 'package:cabbage_studio/frames/personal_account_page.dart';
import 'package:cabbage_studio/frames/project_messages_page.dart';
import 'package:cabbage_studio/frames/your_projects_page.dart';
import 'package:cabbage_studio/frames/news_page.dart';
import 'package:cabbage_studio/pages/admin/admin_page.dart';
import 'package:cabbage_studio/pages/admin/admin_projects_page.dart';
import 'package:cabbage_studio/frames/welcome_page.dart';
import 'package:cabbage_studio/templates/about_us_page.dart';
import 'package:cabbage_studio/templates/main_page.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var currentUser = 'UserId1';
var isUser = true;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  db.isWeb = kIsWeb;
  db.projectId = 'the-cabbage-studio';
  var auth = FirebaseAuth.instanceFor(app: await Firebase.initializeApp());
  if (auth.currentUser == null) await auth.signInAnonymously();
  var user = auth.currentUser;
  user = auth.currentUser;

  db.realtimeIdTokenGenerator = user.getIdToken;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // onGenerateRoute: (route) {
        //   print(route.name);
        //   return MaterialPageRoute(builder: (_) => WelcomePage());
        // }
        routes: {'/admin': (_) => AdminPage()},
        title: 'Cabbage Studio',
        theme: ThemeData(
            textTheme: GoogleFonts.sourceCodeProTextTheme().apply(
                // fontFamily: 'SourceCodePro',
                bodyColor: Colors.grey,
                displayColor: Colors.grey),
            primarySwatch: Colors.grey),
        home: WelcomePage());
  }
}

import 'dart:math';

import 'package:cabbage_studio/frames/all_projects_messages.dart';
import 'package:cabbage_studio/frames/news_page.dart';
import 'package:cabbage_studio/frames/your_projects_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart';

import 'empty_containers.dart';
import 'pages_templates.dart';

class DrawerWidget extends StatefulWidget {
  final double width;
  final double height;
  final double depth;
  final withDrawerLegs;

  const DrawerWidget(this.width, this.height, this.depth, this.withDrawerLegs,
      {Key key})
      : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  Size _boxSize;
  final _drawerElementOffset = Offset(-8.0, -8.0);
  double _paddingForDrawerElements;

  // Sum of drawerLegHeightQuotient and drawerHeightQuotient is 1.0
  final drawerHeightQuotient = 0.775;
  final drawerLegHeightQuotient = 0.225;

  @override
  Widget build(BuildContext context) {
    final _drawerHeightWithoutLegs = widget.height * drawerHeightQuotient;
    // _boxSize = Size(widget.width / 1.15, 48.0);
    _boxSize = Size(widget.width, 55.0);
    _paddingForDrawerElements =
        paddingForDrawerElements(_drawerHeightWithoutLegs, 4, 3);
    return drawer(widget.width, _drawerHeightWithoutLegs, widget.depth);
  }

  paddingForDrawerElements(
      double drawerHeightWithoutLegs, int boxElements, int numberOfSpaces) {
    return (drawerHeightWithoutLegs - (boxElements * _boxSize.height)) /
        numberOfSpaces;
  }

  drawer(double width, double height, double depth) {
    ZToBoxAdapter boxContainer() {
      return ZToBoxAdapter(
          width: width, height: height, child: Center(child: emptyContainer()));
    }

    drawerLeg(legWidth) {
      return ZShape(path: [
        ZMove(-legWidth / 2, -height * drawerLegHeightQuotient / 2, 0.0),
        ZLine(-legWidth / 2, height * drawerLegHeightQuotient / 2, 0.0),
        ZLine(legWidth / 2, height * drawerLegHeightQuotient / 2, 0.0),
        ZLine(legWidth / 2, -height * drawerLegHeightQuotient / 2, 0.0),
        ZLine(-legWidth / 2, -height * drawerLegHeightQuotient / 2, 0.0),
        ZMove(legWidth / 2 - legWidth / 3, height * drawerLegHeightQuotient / 2,
            0.0),
        ZLine(legWidth / 2 - legWidth / 3,
            -height * drawerLegHeightQuotient / 2, 0.0),
      ], closed: false, color: Colors.black);
    }

    return ZGroup(sortMode: SortMode.stack, children: [
      ZPositioned(
          translate: ZVector(
              -width / 2 + width * 0.11 / 2,
              height / 2 + height * drawerLegHeightQuotient / 2,
              depth / 2 + 1.0),
          child: drawerLeg(width * 0.11)),
      ZPositioned(
          translate: ZVector(
              width / 2 - width * 0.11 / 2,
              height / 2 + height * drawerLegHeightQuotient / 2,
              depth / 2 + 1.0),
          child: drawerLeg(width * 0.11)),
      ZPositioned(
          translate: ZVector(
              width / 2 - width * 0.11 / 2,
              height / 2 + height * drawerLegHeightQuotient / 2,
              -depth / 2 + 1.0),
          child: drawerLeg(width * 0.11)),
      ZGroup(children: [
        ZPositioned(
            translate: ZVector(-width / 2 + 1.0, 0, 0),
            rotate: ZVector(0, pi / 2, 0),
            child: ZToBoxAdapter(
                width: depth,
                height: height,
                child: Center(child: emptyContainer()))),
        ZPositioned(
            translate: ZVector(0, 0, -depth / 2 + 1.0), child: boxContainer()),
        ZBox(
            height: height,
            width: width,
            depth: depth,
            color: Colors.black,
            fill: false),
        ZPositioned(
            translate: ZVector(0, -height / 2 + 0.5, 0),
            rotate: ZVector(pi / 2, 0, 0),
            child: ZToBoxAdapter(
                width: width - 2,
                height: depth,
                child: Container(color: Colors.white))),
        ZPositioned(
            translate: ZVector(0, 0, depth / 2 + 1.0),
            child: ZToBoxAdapter(
                width: width + 1.5,
                height: height,
                child: Center(child: emptyContainer()))),
        ZPositioned(
            translate: ZVector(width / 2, 2, 7),
            rotate: ZVector(0, pi / 2, 0),
            child: ZToBoxAdapter(
                width: depth - 15,
                height: height - 5,
                child: Center(child: emptyContainer()))),
      ]),
      ZGroup(children: [
        ZPositioned(
            translate:
                ZVector(0, -height / 2 + _boxSize.height / 2, depth / 2 + 1.0),
            child: BoxElement('Ваши Новости', _boxSize.width, _boxSize.height,
                _drawerElementOffset, null, 1, () {
              goToPage(context, this.widget, NewsPage());
            })),
        ZPositioned(
            translate: ZVector(
                0,
                -height / 2 + _boxSize.height * 1.5 + _paddingForDrawerElements,
                depth / 2 + 1.0),
            child: BoxElement('Проекты', _boxSize.width, _boxSize.height,
                _drawerElementOffset, null, 1, () {
              goToPage(context, this.widget, YourProjectsPage());
            })),
        ZPositioned(
            translate: ZVector(
                0,
                height / 2 - _boxSize.height * 1.5 - _paddingForDrawerElements,
                depth / 2 + 1.0),
            child: BoxElement('Сообщения', _boxSize.width, _boxSize.height,
                _drawerElementOffset, null, 1, () {
              goToPage(context, this.widget, AllProjectMessages());
            })),
        ZPositioned(
            translate: ZVector(0, height / 2 - _boxSize.height / 2, depth / 2),
            child: BoxElement(
                'О Вас', _boxSize.width, _boxSize.height, _drawerElementOffset))
      ]),
    ]);
  }
}

class BoxElement extends StatefulWidget {
  final String title;
  final double boxWidth;
  final double boxHeight;
  final Offset frontBoxOpenOffset;
  final dynamic bottomContent;
  final double drawerStroke;
  final Function drawerImplementation;

  const BoxElement(
      this.title, this.boxWidth, this.boxHeight, this.frontBoxOpenOffset,
      [this.bottomContent,
      this.drawerStroke = 1.0,
      this.drawerImplementation,
      Key key])
      : super(key: key);

  @override
  _BoxElementState createState() => _BoxElementState();
}

class _BoxElementState extends State<BoxElement>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  Tween _tween;

  var _isOpened = false;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    _tween = Tween(
        begin: Offset(widget.frontBoxOpenOffset.dx * 0.6,
            widget.frontBoxOpenOffset.dy * 0.6),
        end: widget.frontBoxOpenOffset);
    _animation = _tween.animate(
        CurvedAnimation(curve: Curves.easeInQuint, parent: _controller));

    _controller.addStatusListener((status) async {
      if (status == AnimationStatus.completed &&
          _animation.value == widget.frontBoxOpenOffset) {
        await Future.delayed(Duration(milliseconds: 300));
        animationReverse();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context, Widget child) {
          return ZGroup(children: [
            // Box
            // ZToBoxAdapter(
            //     width: widget.boxWidth + 1,
            //     height: widget.boxHeight + 1,
            //     child: Container(color: Colors.white)),
            ZShape(path: [
              // Front top left
              ZMove(-widget.boxWidth / 2 + _animation.value.dx,
                  -widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Front top right
              ZLine(widget.boxWidth / 2 + _animation.value.dx,
                  -widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Front bottom right
              ZLine(widget.boxWidth / 2 + _animation.value.dx,
                  widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Front bottom left
              ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                  widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Return front top left
              ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                  -widget.boxHeight / 2 + _animation.value.dy, 0.0),
              //Connection between back bottom left and front bottom left
              ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              ZLine(-widget.boxWidth / 2 + _animation.value.dx,
                  widget.boxHeight / 2 + _animation.value.dy, 0.0),
              //Connection between back bottom left and front bottom line
              ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              ZLine(-widget.boxWidth / 2,
                  widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Connection between bottom lines
              ZMove(-widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              ZLine(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              // Connection between back bottom right and front bottom right
              ZMove(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              ZLine(widget.boxWidth / 2 + _animation.value.dx,
                  widget.boxHeight / 2 + _animation.value.dy, 0.0),
              // Connection between back bottom right and back top right
              ZMove(widget.boxWidth / 2, widget.boxHeight / 2, 0.0),
              ZLine(widget.boxWidth / 2, -widget.boxHeight / 2, 0.0),
              // Connection between back top right and front top left
              ZMove(widget.boxWidth / 2, -widget.boxHeight / 2, 0.0),
              ZLine(widget.boxWidth / 2 + _animation.value.dx,
                  -widget.boxHeight / 2, 0.0),
            ], color: Colors.black, stroke: widget.drawerStroke, closed: false),

            ZPositioned(
                translate:
                    ZVector(_animation.value.dx, _animation.value.dy, 0.0),
                child: ZToBoxAdapter(
                    width: widget.boxWidth - widget.drawerStroke,
                    height: widget.boxHeight - widget.drawerStroke,
                    child: Material(
                      color: Colors.white,
                      child: InkWell(
                          onTap: () {
                            if (widget.drawerImplementation != null) {
                              widget.drawerImplementation();
                            }
                            if (!_isOpened) {
                              animationForward();
                            } else if (_isOpened) {
                              animationReverse();
                            }
                          },
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(widget.title,
                                      style: GoogleFonts.sourceCodePro(
                                          color: Colors.black,
                                          fontSize: 18))))),
                    ))),
          ]);
        });
  }

  animationReverse() {
    _controller.reverse();
    _isOpened = false;
  }

  animationForward() {
    _controller.forward();
    _isOpened = true;
  }
}

import 'package:cabbage_studio/frames/all_projects_messages.dart';
import 'package:cabbage_studio/frames/menu_page.dart';
import 'package:cabbage_studio/transitions/enter_exit_route.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

byCabbageStudio([bool isBold = true, Function buttonImplementation]) {
  return SizedBox(
      width: 118.0,
      height: 38.0,
      child: InkWell(
        borderRadius: BorderRadius.circular(18.0),
        onTap: () {
          buttonImplementation();
        },
        child: Stack(
            alignment: Alignment.centerLeft,
            overflow: Overflow.visible,
            children: [
              Material(
                  color: Colors.transparent,
                  child: Container(
                      decoration: BoxDecoration(
                          // color: Colors.white,
                          border: Border.all(
                              color: Color.fromRGBO(227, 227, 227, 1),
                              width: 2.0),
                          borderRadius:
                              BorderRadius.all(Radius.circular(26.0))))),
              Positioned(
                  left: 5.0,
                  top: -11.0,
                  child: Row(children: [
                    Text('by',
                        style: GoogleFonts.sourceCodePro(
                            fontSize: 20,
                            color: Colors.black.withOpacity(0.5))),
                    Text('Cabbage\nStudio',
                        style: TextStyle(
                            fontFamily: 'SourceCodePro',
                            fontSize: 24.0,
                            fontStyle: FontStyle.italic,
                            fontWeight:
                                isBold ? FontWeight.w600 : FontWeight.w300,
                            color: Colors.black.withOpacity(0.5),
                            shadows: [
                              Shadow(
                                  offset: Offset(1, 0.6),
                                  blurRadius: 1.0,
                                  color: Colors.black)
                            ]))
                  ])),
            ]),
      ));
}

bottomTitle(context, title) {
  return Text(title,
      style: GoogleFonts.sourceCodePro(
          color: Colors.black, fontWeight: FontWeight.normal, fontSize: 20));
}

goToPage(context, pageFrom, pageTo) {
  // Navigator.push(context, MaterialPageRoute(builder: (context) => pageTo));
  Navigator.push(
      context, EnterExitRoute(exitPage: pageFrom, enterPage: pageTo));
}

import 'dart:math';

import 'package:cabbage_studio/frames/hero_content_page.dart';
import 'package:cabbage_studio/frames/hero_project_page.dart';
import 'package:cabbage_studio/frames/project_messages_page.dart';
import 'package:cabbage_studio/frames/project_detailed_page.dart';
import 'package:cabbage_studio/frames/your_projects_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_cabbage_studio/db_cabbage_studio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zflutter/zflutter.dart';
import 'package:flutter_svg/svg.dart';

import '../main.dart';
import 'pages_templates.dart';

class ProjectsBoxPortfolio extends StatefulWidget {
  final currentPageWidget;
  final projects;

  const ProjectsBoxPortfolio(this.currentPageWidget, this.projects, {Key key})
      : super(key: key);

  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<ProjectsBoxPortfolio>
    with SingleTickerProviderStateMixin {
  AnimationController _boxRotationAnimationController;
  Tween _boxRotationTween;
  Animation _boxRotationAnimation;

  var _rotateAngle = 0.0;
  var _rotatesNumber = 0;
  var _currentFace = 1;

  var _switcherDuration = Duration(milliseconds: 600);

  @override
  void initState() {
    // for (var project in widget.projects) {
    //   print(project.photoUrl.value);
    // }

    _boxRotationAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 900));

    _boxRotationTween = Tween<double>(begin: 0.0, end: 0.0);
    _boxRotationAnimation = _boxRotationTween.animate(CurvedAnimation(
        curve: Curves.easeInQuint, parent: _boxRotationAnimationController));

    _boxRotationAnimationController.addListener(() {
      _rotateAngle = _boxRotationAnimation.value;
    });

    _boxRotationAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _rotatesNumber = (_rotateAngle ~/ (pi / 2));
        _currentFace = (_rotatesNumber) % 4 + 1;
        setState(() {});
      }
    });

    super.initState();
  }

  closestAngle(double a, double b) {
    var c1 = a - (a % b);
    var c2 = (a + b) - (a % b);
    if (a - c1 > c2 - a) {
      return c2;
    } else {
      return c1;
    }
  }

  @override
  Widget build(BuildContext context) {
    // print('Output: ${widget.projects}');
    // for (var project in widget.projects) {
    //   print(project.photoUrl.value);
    // }
    // print(widget?.projects?.value[0]?.photoUrl?.value);

    // print();
    // print(widget.projects.runtimeType);
    // print(widget.projects.values[0].photoUrl.value);
    var pageSize = MediaQuery.of(context).size;
    return ZPositioned(
        rotate: ZVector(-0.125, 0.145, 0.0),
        child: ZGroup(children: [
          AnimatedBuilder(
              animation: _boxRotationAnimationController,
              child: ZGroup(children: [
                ZPositioned(
                    translate: ZVector.only(y: -100),
                    child: topBox(235, 340, 235)),
                ZPositioned(
                    translate: ZVector.only(y: 145),
                    child: middleBox(235, 160, 235)),
                ZPositioned(
                    translate: ZVector.only(y: 250),
                    child: bottomBox(235, 65, 235))
              ]),
              builder: (BuildContext context, Widget child) {
                return ZPositioned(
                    rotate: ZVector(-0.0, _rotateAngle, 0.0), child: child);
              }),
          ZPositioned(
              translate: ZVector(340, -90, 0),
              child: ZToBoxAdapter(
                  height: 310,
                  width: 400,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(width: 8.0),
                        Padding(
                            padding: const EdgeInsets.only(top: 13.0),
                            child: kIsWeb
                                // ? Image.network('assets/icons/right-arrow.svg',
                                //     width: 22.0, height: 22.0)
                                ? Image.network('assets/icons/right-arrow.svg',
                                    width: 22.0, height: 22.0)
                                : SvgPicture.asset(
                                    'assets/icons/right-arrow.svg',
                                    width: 22.0,
                                    height: 22.0)),
                        SizedBox(width: 8.0),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => _currentFace ==
                                                    1
                                                ? HeroProjectPage(
                                                    'https://the-cabbage-studio.web.app/projects/theater/')
                                                : HeroContentPage(_currentFace ==
                                                        1
                                                    ? Container(
                                                        color: Colors.cyan)
                                                    : _currentFace == 2
                                                        ? Container(
                                                            color:
                                                                Colors.orange)
                                                        : _currentFace == 3
                                                            ? Container(
                                                                color: Colors
                                                                    .yellow)
                                                            : Container(
                                                                color: Colors
                                                                    .green))));
                                  },
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('открыть проект',
                                          style: GoogleFonts.sourceCodePro(
                                              // fontFamily: 'SourceCodePro',
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black)))),
                              InkWell(
                                  onTap: () {
                                    goToPage(
                                        context,
                                        widget.currentPageWidget,
                                        ProjectDetailedPage(widget?.projects
                                            ?.elementAt(_currentFace - 1)));
                                  },
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('подробнее о проекте',
                                          style: GoogleFonts.sourceCodePro(
                                              // fontFamily: 'SourceCodePro',
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black)))),
                              InkWell(
                                  onTap: () {
                                    goToPage(
                                        context,
                                        widget.currentPageWidget,
                                        ProjectDetailedPage(widget?.projects
                                            ?.elementAt(_currentFace - 1)));
                                  },
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('дополнительные материалы',
                                          style: GoogleFonts.sourceCodePro(
                                              // fontFamily: 'SourceCodePro',
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black)))),
                              Spacer(),
                              InkWell(
                                  onTap: () {
                                    goToPage(
                                        context,
                                        widget.currentPageWidget,
                                        ProjectMessagesPage(
                                            widget?.projects
                                                ?.elementAt(_currentFace - 1),
                                            isUser));
                                  },
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('задать вопрос',
                                          style: GoogleFonts.sourceCodePro(
                                              // fontFamily: 'SourceCodePro',
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black))))
                            ])
                      ]))),
          ZPositioned(
              translate: ZVector.only(x: -12.0, y: pageSize.height / 2 - 40),
              // translate: ZVector.only(x: -150.0, y: pageSize.height / 2 - 40),
              child: ZToBoxAdapter(
                  width: 200,
                  height: 100,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                            onTap: () {
                              rotateBoxBack();
                            },
                            child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Transform.rotate(
                                    angle: pi,
                                    child: kIsWeb
                                        ? Image.network(
                                            'assets/icons/right-arrow.svg',
                                            width: 20.0,
                                            height: 20.0)
                                        : SvgPicture.asset(
                                            'assets/icons/right-arrow.svg',
                                            width: 20.0,
                                            height: 20.0)))),
                        InkWell(
                            onTap: () {
                              rotateBoxForward();
                            },
                            // child: Icon(Icons.arrow_right_alt, size: 40))
                            child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: kIsWeb
                                    ? Image.network(
                                        'assets/icons/right-arrow.svg',
                                        width: 20.0,
                                        height: 20.0)
                                    : SvgPicture.asset(
                                        'assets/icons/right-arrow.svg',
                                        width: 20.0,
                                        height: 20.0)))
                      ])))
        ]));
  }

  rotateBoxBack() {
    var futureAngle = closestAngle(_rotateAngle, pi / 2) - pi / 2;
    rotateBox(futureAngle);
  }

  rotateBoxForward() {
    var futureAngle = closestAngle(_rotateAngle, pi / 2) + pi / 2;
    rotateBox(futureAngle);
  }

  rotateBox(futureAngle) {
    _boxRotationTween.begin = _rotateAngle;
    _boxRotationTween.end = futureAngle;
    _boxRotationAnimationController
      ..reset()
      ..forward();
  }

  emptyTransparentContainer() {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: Container(color: Colors.transparent));
  }

  emptyContainer() {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: Container(color: Colors.white));
  }

  topBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Widget content) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Stack(children: [emptyContainer(), content]));
    }

    topBoxSwitcher(faceStatement) {
      return AnimatedSwitcher(
          duration: _switcherDuration,
          child: _currentFace == faceStatement &&
                  widget?.projects != null &&
                  widget.projects.length >= faceStatement
              ? Hero(
                  tag: 'heroAnimation',
                  child: CachedNetworkImage(
                      imageUrl: widget?.projects
                              ?.elementAt(faceStatement - 1)
                              ?.photoUrl
                              ?.value ??
                          '',
                      fit: BoxFit.cover,
                      height: height,
                      alignment: Alignment.center))
              : emptyContainer());
    }

    return ZGroup(children: [
      ZPositioned(
          translate: ZVector(-width / 2 + 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, topBoxSwitcher(4))),
      ZPositioned(
          translate: ZVector(width / 2 - 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, topBoxSwitcher(2))),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2 + 1.0),
          child: boxContainer(width, height, topBoxSwitcher(3))),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2 - 1.0),
          child: boxContainer(width, height, topBoxSwitcher(1))),
      ZPositioned(
          translate: ZVector(0, -height / 2 + 0.5, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: ZToBoxAdapter(
              width: width,
              height: depth,
              child: Container(color: Colors.white))),
      ZBox(
          height: height,
          width: width,
          depth: depth,
          color: Colors.black,
          fill: false)
    ]);
  }

  middleBoxTitle(String description) {
    return Column(
        // mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
              child: Column(children: [
            SizedBox(height: 16.0),
            Text(description,
                style: GoogleFonts.robotoMono(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w300),
                overflow: TextOverflow.ellipsis),
            SizedBox(height: 14.0)
          ])),
          Align(
              alignment: Alignment.centerLeft,
              child: Material(
                  color: Colors.white,
                  child: InkWell(
                    onTap: () {
                      goToPage(
                          context,
                          widget.currentPageWidget,
                          ProjectDetailedPage(
                              widget?.projects?.elementAt(_currentFace - 1)));
                    },
                    child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black)),
                        child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 4.0, horizontal: 6.0),
                            child: RichText(
                                text: TextSpan(
                                    style: GoogleFonts.robotoMono(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                    children: <TextSpan>[
                                  TextSpan(
                                      text: 'читать больше',
                                      style: TextStyle(
                                          decoration:
                                              TextDecoration.underline)),
                                  TextSpan(text: '...')
                                ])))),
                  )))
        ]);
  }

  middleBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Widget content) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Padding(
              padding:
                  const EdgeInsets.only(left: 6.0, right: 6.0, bottom: 24.0),
              child: content));
    }

    middleBoxSwitcher(faceStatement) {
      return AnimatedSwitcher(
          duration: _switcherDuration,
          child: _currentFace == faceStatement &&
                  widget?.projects != null &&
                  widget.projects.length >= faceStatement
              ? middleBoxTitle(widget?.projects
                      ?.elementAt(faceStatement - 1)
                      ?.status
                      ?.value ??
                  '')
              : emptyTransparentContainer());
    }

    return ZGroup(children: [
      ZPositioned(
          translate: ZVector(-width / 2, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, middleBoxSwitcher(4))),
      ZPositioned(
          translate: ZVector(width / 2, 0, 0),
          rotate: ZVector(0, -pi / 2, 0),
          child: boxContainer(depth, height, middleBoxSwitcher(2))),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2),
          rotate: ZVector(0, -pi, 0),
          child: boxContainer(width, height, middleBoxSwitcher(3))),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2),
          child: boxContainer(width, height, middleBoxSwitcher(1)))
    ]);
  }

  bottomBoxTitle(String title) {
    return Container(
        color: Colors.white,
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 14.0),
                child: Text(title,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w400)))));
  }

  bottomBox(double width, double height, double depth) {
    ZToBoxAdapter boxContainer(
        double boxWidth, double boxHeight, Widget content) {
      return ZToBoxAdapter(
          width: boxWidth,
          height: boxHeight,
          child: Stack(children: [
            emptyContainer(),
            Padding(padding: const EdgeInsets.all(1.0), child: content)
          ]));
    }

    bottomBoxSwitcher(faceStatement) {
      return AnimatedSwitcher(
          duration: _switcherDuration,
          child: _currentFace == faceStatement &&
                  widget?.projects != null &&
                  widget.projects.length >= faceStatement
              ? bottomBoxTitle(
                  widget?.projects?.elementAt(_currentFace - 1)?.name?.value ??
                      '')
              : emptyContainer());
    }

    return ZGroup(children: [
      ZPositioned(
          translate: ZVector(-width / 2 + 1.0, 0, 0),
          rotate: ZVector(0, pi / 2, 0),
          child: boxContainer(depth, height, bottomBoxSwitcher(4))),
      ZPositioned(
          translate: ZVector(width / 2 - 1.0, 0, 0),
          rotate: ZVector(0, -pi / 2, 0),
          child: boxContainer(depth, height, bottomBoxSwitcher(2))),
      ZPositioned(
          translate: ZVector(0, 0, -depth / 2 + 1.0),
          rotate: ZVector(0, -pi, 0),
          child: boxContainer(width, height, bottomBoxSwitcher(3))),
      ZPositioned(
          translate: ZVector(0, 0, depth / 2 - 1.0),
          child: boxContainer(width, height, bottomBoxSwitcher(1))),
      ZPositioned(
          translate: ZVector(0, -height / 2 + 1.0, 0),
          rotate: ZVector(pi / 2, 0, 0),
          child: boxContainer(width, depth, emptyContainer())),
      ZBox(
          height: height,
          width: width,
          depth: depth,
          color: Colors.black,
          fill: false)
    ]);
  }
}
